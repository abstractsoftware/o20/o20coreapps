<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>O20Gui</name>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="111"/>
        <source>Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="456"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Word&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="481"/>
        <location filename="../o20.word/O20Ui.ui" line="487"/>
        <location filename="../o20.word/O20Ui.ui" line="883"/>
        <location filename="../o20.word/O20Ui.ui" line="5013"/>
        <source>Home</source>
        <translation type="unfinished">Zuhause</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="537"/>
        <location filename="../o20.word/O20Ui.ui" line="543"/>
        <location filename="../o20.word/O20Ui.ui" line="927"/>
        <location filename="../o20.word/O20Ui.ui" line="1933"/>
        <location filename="../o20.word/O20Ui.ui" line="2071"/>
        <source>New</source>
        <translation type="unfinished">Neu</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="587"/>
        <location filename="../o20.word/O20Ui.ui" line="593"/>
        <location filename="../o20.word/O20Ui.ui" line="968"/>
        <location filename="../o20.word/O20Ui.ui" line="3108"/>
        <location filename="../o20.word/TextGuiCommands.cpp" line="925"/>
        <source>Open</source>
        <translation type="unfinished">Öffnen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="664"/>
        <location filename="../o20.word/O20Ui.ui" line="1122"/>
        <location filename="../o20.word/O20Ui.ui" line="3732"/>
        <source>Options</source>
        <translation type="unfinished">Optionen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="845"/>
        <source>Back</source>
        <translation type="unfinished">Zurück</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1012"/>
        <location filename="../o20.word/O20Ui.ui" line="3181"/>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1087"/>
        <source>About</source>
        <translation type="unfinished">Information</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1427"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Welcome.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Herzlich willkommen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1463"/>
        <source>Save Pending</source>
        <translation type="unfinished">Speichern ausstehend</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1469"/>
        <source>  Click here to save this document.            </source>
        <translation type="unfinished">  Klicken Sie hier, um dieses Dokument zu speichern.            </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1495"/>
        <source>  This document is ReadOnly.            </source>
        <translation type="unfinished">  Dieses Dokument ist ReadOnly.            </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1542"/>
        <source>  Spectrum is AutoSaving this document.            </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1625"/>
        <source>Recent Files</source>
        <translation type="unfinished"> Aktuelle Dateien </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1640"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/icons/rebrand/ms-word.svg&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;You don&apos;t have any recent documents yet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/icons/rebrand/ms-word.svg&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Sie haben noch keine aktuellen Dokumente.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1706"/>
        <source> Documents/O20/Documents1.odt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1715"/>
        <source> Documents/O20/Documents1.html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1724"/>
        <source> Projects/o20coreapps/CMakeLists.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1751"/>
        <source>Open Selected File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1754"/>
        <location filename="../o20.word/O20Ui.ui" line="1768"/>
        <location filename="../o20.word/O20Ui.ui" line="1782"/>
        <location filename="../o20.word/O20Ui.ui" line="1799"/>
        <location filename="../o20.word/O20Ui.ui" line="2224"/>
        <location filename="../o20.word/O20Ui.ui" line="2244"/>
        <location filename="../o20.word/O20Ui.ui" line="2297"/>
        <location filename="../o20.word/O20Ui.ui" line="2317"/>
        <location filename="../o20.word/O20Ui.ui" line="2448"/>
        <location filename="../o20.word/O20Ui.ui" line="2506"/>
        <location filename="../o20.word/O20Ui.ui" line="2526"/>
        <location filename="../o20.word/O20Ui.ui" line="2565"/>
        <location filename="../o20.word/O20Ui.ui" line="3963"/>
        <location filename="../o20.word/O20Ui.ui" line="4005"/>
        <location filename="../o20.word/O20Ui.ui" line="4034"/>
        <location filename="../o20.word/O20Ui.ui" line="4063"/>
        <location filename="../o20.word/O20Ui.ui" line="4092"/>
        <location filename="../o20.word/O20Ui.ui" line="4121"/>
        <location filename="../o20.word/O20Ui.ui" line="4150"/>
        <location filename="../o20.word/O20Ui.ui" line="4179"/>
        <location filename="../o20.word/O20Ui.ui" line="4208"/>
        <location filename="../o20.word/O20Ui.ui" line="4237"/>
        <location filename="../o20.word/O20Ui.ui" line="4266"/>
        <location filename="../o20.word/O20Ui.ui" line="4295"/>
        <location filename="../o20.word/O20Ui.ui" line="4324"/>
        <location filename="../o20.word/O20Ui.ui" line="4353"/>
        <location filename="../o20.word/O20Ui.ui" line="4382"/>
        <location filename="../o20.word/O20Ui.ui" line="4411"/>
        <location filename="../o20.word/O20Ui.ui" line="4440"/>
        <location filename="../o20.word/O20Ui.ui" line="4469"/>
        <location filename="../o20.word/O20Ui.ui" line="4753"/>
        <location filename="../o20.word/O20Ui.ui" line="4858"/>
        <location filename="../o20.word/O20Ui.ui" line="5552"/>
        <location filename="../o20.word/O20Ui.ui" line="5830"/>
        <location filename="../o20.word/O20Ui.ui" line="5859"/>
        <location filename="../o20.word/O20Ui.ui" line="5882"/>
        <location filename="../o20.word/O20Ui.ui" line="5899"/>
        <location filename="../o20.word/O20Ui.ui" line="5973"/>
        <location filename="../o20.word/O20Ui.ui" line="5992"/>
        <location filename="../o20.word/O20Ui.ui" line="6079"/>
        <location filename="../o20.word/O20Ui.ui" line="6125"/>
        <location filename="../o20.word/O20Ui.ui" line="6136"/>
        <location filename="../o20.word/O20Ui.ui" line="6345"/>
        <location filename="../o20.word/O20Ui.ui" line="6365"/>
        <location filename="../o20.word/O20Ui.ui" line="6376"/>
        <location filename="../o20.word/O20Ui.ui" line="6387"/>
        <location filename="../o20.word/O20Ui.ui" line="6398"/>
        <location filename="../o20.word/O20Ui.ui" line="6506"/>
        <location filename="../o20.word/O20Ui.ui" line="6526"/>
        <location filename="../o20.word/O20Ui.ui" line="6553"/>
        <location filename="../o20.word/O20Ui.ui" line="7875"/>
        <location filename="../o20.word/O20Ui.ui" line="7914"/>
        <location filename="../o20.word/O20Ui.ui" line="7981"/>
        <location filename="../o20.word/O20Ui.ui" line="8023"/>
        <location filename="../o20.word/O20Ui.ui" line="8122"/>
        <location filename="../o20.word/O20Ui.ui" line="8167"/>
        <location filename="../o20.word/O20Ui.ui" line="8196"/>
        <location filename="../o20.word/O20Ui.ui" line="8232"/>
        <location filename="../o20.word/O20Ui.ui" line="8261"/>
        <location filename="../o20.word/O20Ui.ui" line="8362"/>
        <location filename="../o20.word/O20Ui.ui" line="8446"/>
        <location filename="../o20.word/O20Ui.ui" line="8511"/>
        <location filename="../o20.word/O20Ui.ui" line="8685"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1779"/>
        <source>Remove From List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1796"/>
        <location filename="../o20.word/TextGuiCommands.cpp" line="928"/>
        <source>Open File Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1883"/>
        <source>Open Existing Document</source>
        <translation type="unfinished">Vorhandenes Dokument öffnen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1889"/>
        <location filename="../o20.word/O20Ui.ui" line="2734"/>
        <source>Browse</source>
        <translation type="unfinished">Durchsuche</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="1927"/>
        <source>New Blank Document</source>
        <translation type="unfinished">Neues leeres Dokument</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2490"/>
        <source>Student report</source>
        <translation type="unfinished">Studentenbericht</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2543"/>
        <source>General notes</source>
        <translation type="unfinished">Allgemeine Hinweise</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2334"/>
        <source>Resume</source>
        <translation type="unfinished">Resümee</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2376"/>
        <source>Resume cover letter
(chronological)</source>
        <translation type="unfinished">Anschreiben fortsetzen (chronologisch)</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2267"/>
        <source>Bold Report</source>
        <translation type="unfinished">Mutiger Bericht</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2471"/>
        <source>   Simple Document
   Example of how styles and formatting
   can make a document in O20.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2419"/>
        <source>Business letter</source>
        <translation type="unfinished">Geschäftsbrief</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2280"/>
        <source>Resume
(chronological)</source>
        <translation type="unfinished">Lebenslauf (chronologisch)</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2347"/>
        <source>Basic blank</source>
        <translation type="unfinished">Grundleeres Leerzeichen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2432"/>
        <source>Resume cover letter</source>
        <translation type="unfinished">Anschreiben fortsetzen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2604"/>
        <source>Blank Doc</source>
        <translation type="unfinished">Leeres Dokument</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2585"/>
        <source>Single spaced</source>
        <translation type="unfinished">Einzelner Raum</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2098"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2121"/>
        <source>Blank document</source>
        <translation type="unfinished">Leeres Dokument</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2737"/>
        <source>  Browse</source>
        <translation type="unfinished">  Durchsuche</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2750"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2763"/>
        <source>  Import Code</source>
        <translation type="unfinished">  Code importieren</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2789"/>
        <source>Open Location</source>
        <translation type="unfinished">Standort öffnen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2792"/>
        <source>  Open Location</source>
        <translation type="unfinished">  Standort öffnen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2836"/>
        <location filename="../o20.word/TextGuiCommands.cpp" line="926"/>
        <source>Open ReadOnly</source>
        <translation type="unfinished">Öffnen Sie ReadOnly</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="2971"/>
        <source> Browse </source>
        <translation type="unfinished"> Durchsuche </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3026"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3035"/>
        <source>Folder 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3044"/>
        <source>Folder 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3053"/>
        <source>File.odt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3062"/>
        <source>Main.cpp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3071"/>
        <source>Website.html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3153"/>
        <source>  Save as PDF</source>
        <translation type="unfinished">  Als PDF speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3196"/>
        <source>  Save As</source>
        <translation type="unfinished">  Speichern als</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3225"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;WARNING: O20 CANNOT OPEN DOC OR DOCX!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;WARNUNG: O20 KANN DOC ODER DOCX NICHT ÖFFNEN!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3267"/>
        <source>  Save</source>
        <translation type="unfinished">  Speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3287"/>
        <source>  Print</source>
        <translation type="unfinished">  Drucken</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;About O20&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Über O20&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Not getting updates for O20? Try running:&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Ubuntu Mono&apos;; font-size:11pt;&quot;&gt;&amp;gt; snap refresh office20&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Keine Updates für O20? Versuche zu rennen&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Ubuntu Mono&apos;; font-size:11pt;&quot;&gt;&amp;gt; snap refresh office20&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3739"/>
        <source>Some of these options may require restart to take effect.</source>
        <translation type="unfinished">Einige dieser Optionen erfordern möglicherweise einen Neustart, um wirksam zu werden.</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3779"/>
        <source>General Settings</source>
        <translation type="unfinished">Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3789"/>
        <source>Assistive Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3799"/>
        <source>Recover the window state</source>
        <translation type="unfinished">Stellen Sie den Fensterstatus wieder her</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3809"/>
        <source>Show the start screen</source>
        <translation type="unfinished">Startbildschirm anzeigen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3819"/>
        <source>Show the splash screen</source>
        <translation type="unfinished">Zeigen Sie den Begrüßungsbildschirm an</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3829"/>
        <source>User Settings and Customization</source>
        <translation type="unfinished">Benutzereinstellungen und Anpassung</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3851"/>
        <source>What would you like me to call you?</source>
        <translation type="unfinished">Wie soll ich dich nennen?</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3873"/>
        <source>What is your full name?</source>
        <translation type="unfinished">Wie lautet Dein voller Name?</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3895"/>
        <source>What are your initials?</source>
        <translation type="unfinished">Was sind deine Initialen?</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3905"/>
        <source>Translate O20</source>
        <translation type="unfinished">Übersetzen Sie O20</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3915"/>
        <source>No Photo</source>
        <translation type="unfinished">Kein Foto</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4495"/>
        <source>Save and AutoSave</source>
        <translation type="unfinished">Speichern und automatisch speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4505"/>
        <source>Always autosave</source>
        <translation type="unfinished">Immer automatisch speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4515"/>
        <source>Ask for save location</source>
        <translation type="unfinished">Fragen Sie nach dem Speicherort</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4525"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Testing features&lt;br/&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Pre-released beta features, may not work properly&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Funktionen testen&lt;br/&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Vorab veröffentlichte Beta-Funktionen funktionieren möglicherweise nicht richtig&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4535"/>
        <source>Extract text from DOCX</source>
        <translation type="unfinished">Extrahieren Sie Text aus DOCX</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4545"/>
        <source>Dark mode for Text and Code</source>
        <translation type="unfinished">Dunkler Modus für Text und Code</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4713"/>
        <source>AutoSave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4733"/>
        <source>qrc:/QML/SwitchButton.qml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4747"/>
        <source>Save Document</source>
        <translation type="unfinished">Dokument speichern</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4805"/>
        <source>Tell me what you want to do</source>
        <translation type="unfinished">Sagen Sie mir, was Sie tun möchten</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="4831"/>
        <source>John Doe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5005"/>
        <source>File</source>
        <translation type="unfinished">Akte</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5105"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5164"/>
        <source>Font Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5177"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5184"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5189"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5194"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5199"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5204"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5209"/>
        <source>13</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5214"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5219"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5224"/>
        <source>18</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5229"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5234"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5239"/>
        <source>26</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5244"/>
        <source>28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5249"/>
        <source>32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5254"/>
        <source>48</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5259"/>
        <source>72</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5291"/>
        <source>Bold
CTRL+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5295"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5308"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5336"/>
        <source>Italic
CTRL+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5353"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5378"/>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5394"/>
        <source>Ctrl+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5410"/>
        <source>Overline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5416"/>
        <source>O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5441"/>
        <source>Strikethrough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5479"/>
        <source>Superscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5520"/>
        <source>Subscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5569"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5601"/>
        <source>Paragraph Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5636"/>
        <source>Clear Formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5690"/>
        <source>Align Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5728"/>
        <source>Align Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5763"/>
        <source>Align Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5798"/>
        <source>Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5827"/>
        <source>Bulleted List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="5926"/>
        <source>Styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6034"/>
        <source>Insert</source>
        <translation type="unfinished">Einfügen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6096"/>
        <source>Insert Picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6099"/>
        <source> Picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6122"/>
        <source>Insert Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6150"/>
        <source>Insert Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6153"/>
        <source> Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6182"/>
        <source>Insert Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6185"/>
        <source> Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6228"/>
        <source>Review</source>
        <translation type="unfinished">Rezension</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6249"/>
        <location filename="../o20.word/O20Ui.ui" line="6252"/>
        <source>Check Spelling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6281"/>
        <location filename="../o20.word/O20Ui.ui" line="6284"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6321"/>
        <location filename="../o20.word/O20Ui.ui" line="6342"/>
        <source>Help</source>
        <translation type="unfinished">Hilfe</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6491"/>
        <source>Hide and Show Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6745"/>
        <source>Spectrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6777"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Spectrum&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/AxIcons/other/png/support-filled-color-wheel_large@1x.png&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Welcome to O20.&lt;br/&gt;This is O365 on Linux.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/AxIcons/other/png/support-filled-color-wheel_large@1x.png&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Willkommen bei O20.&lt;br/&gt;Dies ist O365 unter Linux.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7019"/>
        <source>Welcome to O20.</source>
        <translation type="unfinished">Willkommen bei O20.</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7028"/>
        <source>Goto line 23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7037"/>
        <location filename="../o20.word/SpectrumGui.cpp" line="328"/>
        <source>Done!</source>
        <translation type="unfinished">Erledigt!</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7250"/>
        <location filename="../o20.word/O20Ui.ui" line="8739"/>
        <source>Ask me anything</source>
        <translation type="unfinished">Frag mich was</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7328"/>
        <source> Headings </source>
        <translation type="unfinished"> Überschriften </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7467"/>
        <source> Comments </source>
        <translation type="unfinished"> Bemerkungen </translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7413"/>
        <source> Heading 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3392"/>
        <source>About O20</source>
        <translation type="unfinished">Über O20</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3433"/>
        <source>Abstract
Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3457"/>
        <source>O20 on
Flathub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3481"/>
        <source>O20
Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3505"/>
        <source>Report
Bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3529"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3532"/>
        <source>About
Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3556"/>
        <source>O20 on the
Snap Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="3599"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Open Sans&apos;;&quot;&gt;Icons courtesy of the &lt;/span&gt;&lt;a href=&quot;https://github.com/vinceliuice/McMojave-circle&quot;&gt;&lt;span style=&quot; font-family:&apos;Open Sans&apos;; text-decoration: underline; color:#2980b9;&quot;&gt;McMojave Circle&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Open Sans&apos;;&quot;&gt; and &lt;/span&gt;&lt;a href=&quot;https://www.axialis.com&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; text-decoration: underline; color:#2980b9;&quot;&gt;Axialis Software&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Open Sans&apos;;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="6896"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/undraw_Artificial_intelligence_oyxx.svg.png&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Welcome to O20.&lt;br/&gt;This is O365 on Linux.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/undraw_Artificial_intelligence_oyxx.svg.png&quot;/&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Willkommen bei O20.&lt;br/&gt;Dies ist O365 unter Linux.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7076"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Open Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Open Sans Light&apos;; font-size:18pt;&quot;&gt;What&apos;s New in Office20.Word&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;Welcome to Office20.Word!&lt;br /&gt;Feb2.1 has a lot of new features. Among these are:&lt;br /&gt;- Redesigned Settings page which matches the rest of the UI&lt;br /&gt;- The ability to insert Utf8 characters, which include greek, latin, CJK, and mathmatical symbols.&lt;br /&gt;- A beta DOCX import filter.&lt;br /&gt;- An internal file manager&lt;br /&gt;- Faster AutoSave and Save by taking advantage of parallel processing on modern computers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;Since Jan1.0, we have also added many new things, including:&lt;br /&gt;- Syntax Highlighting for coding and development&lt;br /&gt;- A new splash screen&lt;br /&gt;- A completely rewritten Spectrum engine and GUI&lt;br /&gt;- Reading mode&lt;br /&gt;- Recent files&lt;br /&gt;- O20.Word on Flathub&lt;br /&gt;- And more!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;Don&apos;t hesitate to send a bug report or design idea by emailing us at:&lt;br /&gt;&lt;/span&gt;incoming+abstractsoftware-office20-16112563-issue-@incoming.gitlab.com.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;For more information, visit the following sites:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://abstractsoftware.gitlab.io&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Developer Website&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;a href=&quot;https://gitlab.com/abstractsoftware/office20/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;GitLab Page&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7122"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Open Sans Light&apos;; font-size:18pt;&quot;&gt;Hi! I&apos;m Spectrum.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Open Sans&apos;; font-size:11pt;&quot;&gt;Make the Most of Office20 with me.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;I&apos;m your assistant in Office20.&lt;br /&gt;You can get stuff done faster through me.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Try selecting some text and asking me this:&lt;br /&gt;Make this text italic, Spectrum!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Show file tab&lt;br /&gt;Create a new doc!&lt;br /&gt;What is the time?&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Soon, I will be able to do even more things. You will be able to this:&lt;br /&gt;Go to line 15 and select the entire line.&lt;br /&gt;Replace this line with &amp;quot;I am Spectrum&amp;quot;.&lt;br /&gt;Make the selected text italic and set the text color to be red.&lt;br /&gt;Thanks, Spectrum!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7151"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A free and open source MS Office clone for Linux.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2020 by the Abstract Developers&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is free software; you can redistribute it and/or modify it under the terms of  the GNU General Public License as published by the Free Software Foundation; either version 2 of the  License, or (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General  Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7196"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Open Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Open Sans Light&apos;; font-size:20pt;&quot;&gt;This is O365 on Linux&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Created by the Abstract Developers in Qt5 during the fall of 2019, Office20 is the perfect Office365 alternative for Linux, with a Ribbon-style interface, support for ODF, and Spectrum, the assistant which will allow you to write documents faster.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Now with Word, and PowerPoint and OneNote alternatives coming soon, you can enjoy the experience of Office on your Linux desktop, before it is released in early 2020.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Icons courtesy of &lt;a href=&quot;www.axialis.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Axialis Software&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7436"/>
        <source>qrc:/QML/SwitchButtonLite.qml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7444"/>
        <source>AutoDetect Headings</source>
        <translation type="unfinished">Überschriften automatisch erkennen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7503"/>
        <source> New</source>
        <translation type="unfinished"> Neu</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7615"/>
        <source>John Doe: This is a comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7624"/>
        <source>Editor: This is another comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7633"/>
        <source>Another Editor: And yet another comment...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7653"/>
        <source>Previous versions of this document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7724"/>
        <source>AutoSaved at 1:00pm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7729"/>
        <source>AutoSaved at 1:10pm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7734"/>
        <source>AutoSaved at 1:20pm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7869"/>
        <source>Open in File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7898"/>
        <source>Your document has been AutoSaved as /home/xxx/Documents/O20/Docs/Document1.odt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7911"/>
        <source>Close Notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="7978"/>
        <source>Fatal Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8010"/>
        <source>There was some sort of error doing that. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8119"/>
        <source>Return to File Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8155"/>
        <source>Speak Selected Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8193"/>
        <source>Stop Speaking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8229"/>
        <source>Go into Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8258"/>
        <source>Close Reading Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8349"/>
        <source>0 characters    1 lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8716"/>
        <source>Welcome to O20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8762"/>
        <source>Edit Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Ui.ui" line="8779"/>
        <source>Leave Spectrum-Only Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="61"/>
        <source>O20 Special Characters</source>
        <translation type="unfinished">O20 Sonderzeichen</translation>
    </message>
    <message>
        <source>About O20 %1</source>
        <translation type="obsolete">Über O20 %1</translation>
    </message>
    <message>
        <source>Built with Qt %1</source>
        <translation type="obsolete">Gebaut mit Qt %1</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="260"/>
        <source>Hide Ribbon</source>
        <translation type="unfinished">Farbband ausblenden</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="261"/>
        <source>Show Tabs Only</source>
        <translation type="unfinished">Nur Registerkarten anzeigen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="262"/>
        <source>Show Tabs and Commands</source>
        <translation type="unfinished">Registerkarten und Befehle anzeigen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="263"/>
        <source>Show Sidebar</source>
        <translation type="unfinished">Seitenleiste anzeigen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="277"/>
        <source>Black Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="278"/>
        <source>White Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="279"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="295"/>
        <source>No underline</source>
        <translation type="unfinished">Keine Unterstreichung</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="330"/>
        <source>URL to insert</source>
        <translation type="unfinished">URL zum Einfügen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="351"/>
        <source>Cut</source>
        <translation type="unfinished">Schnitt</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="352"/>
        <source>Copy</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="353"/>
        <source>Paste</source>
        <translation type="unfinished">Einfügen</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="354"/>
        <source>Paste (Text Only)</source>
        <translation type="unfinished">Einfügen (nur Text)</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="363"/>
        <source>Automatic</source>
        <translation type="unfinished">Automatisch</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="365"/>
        <source>Red</source>
        <translation type="unfinished">Rot</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="368"/>
        <source>Crimson</source>
        <translation type="unfinished">Purpur</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="370"/>
        <source>Magenta</source>
        <translation type="unfinished">Magenta</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="372"/>
        <source>Mauve</source>
        <translation type="unfinished">Mauve</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="375"/>
        <source>Orange</source>
        <translation type="unfinished">Orange</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="377"/>
        <source>Yellow</source>
        <translation type="unfinished">Gelb</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="380"/>
        <source>Pink</source>
        <translation type="unfinished">Rosa</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="382"/>
        <source>Violet</source>
        <translation type="unfinished">Violett</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="384"/>
        <source>Cobalt</source>
        <translation type="unfinished">Kobalt</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="387"/>
        <source>Indigo</source>
        <translation type="unfinished">Indigo</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="389"/>
        <source>Teal</source>
        <translation type="unfinished">Blaugrün</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="391"/>
        <source>Steel</source>
        <translation type="unfinished">Stahl</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="393"/>
        <source>Emerald</source>
        <translation type="unfinished">Smaragd</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="395"/>
        <source>Olive</source>
        <translation type="unfinished">Olive</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="398"/>
        <source>Green</source>
        <translation type="unfinished">Grün</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="400"/>
        <source>Cyan</source>
        <translation type="unfinished">Cyan</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="402"/>
        <source>Taupe</source>
        <translation type="unfinished">Taupe</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="405"/>
        <location filename="../o20.word/O20Gui.cpp" line="423"/>
        <source>Select Color...</source>
        <translation type="unfinished">Wähle Farbe...</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="407"/>
        <location filename="../o20.word/O20Gui.cpp" line="425"/>
        <source>Select Color</source>
        <translation type="unfinished">Wähle Farbe</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="429"/>
        <source>Transparent</source>
        <translation type="unfinished">Transparent</translation>
    </message>
    <message>
        <location filename="../o20.word/O20Gui.cpp" line="553"/>
        <source>%1 pages    %2 words  </source>
        <translation type="unfinished">%1 Seiten    %2 Wörter  </translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="58"/>
        <location filename="../o20.word/SpectrumGui.cpp" line="102"/>
        <source>Welcome back!</source>
        <translation type="unfinished">Willkommen zurück!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="59"/>
        <location filename="../o20.word/SpectrumGui.cpp" line="104"/>
        <source>Welcome back, %1!</source>
        <translation type="unfinished">Willkommen zurück, %1!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="62"/>
        <source>italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="62"/>
        <source>bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="62"/>
        <source>strikeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="62"/>
        <source>underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="63"/>
        <source>go to #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="63"/>
        <source>go to line #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="63"/>
        <source>go to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="63"/>
        <source>go to bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="64"/>
        <source>align left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="64"/>
        <source>align right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="64"/>
        <source>align center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="64"/>
        <source>align justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="64"/>
        <source>left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="65"/>
        <source>right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="65"/>
        <source>center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="65"/>
        <source>justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="65"/>
        <source>rename PATH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="96"/>
        <source>Hi.</source>
        <translation type="unfinished">Hallo.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="96"/>
        <source>Hello there.</source>
        <translation type="unfinished">Hallo.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="96"/>
        <source>This is O20.</source>
        <translation type="unfinished">Das ist O20.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <source>Hello!</source>
        <translation type="unfinished">Hallo!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <source>Greetings!</source>
        <translation type="unfinished">Schöne Grüße!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <source>Hey there!</source>
        <translation type="unfinished">Sie da!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <location filename="../o20.word/SpectrumGui.cpp" line="101"/>
        <source>Welcome.</source>
        <translation type="unfinished">Herzlich willkommen.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <source>Good day.</source>
        <translation type="unfinished">Schönen Tag.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="97"/>
        <source>Let&apos;s get started.</source>
        <translation type="unfinished">Lass uns anfangen.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="413"/>
        <source>Something went wrong doing that.</source>
        <translation type="unfinished">Dabei ist etwas schief gelaufen.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="419"/>
        <source>I can&apos;t take you there!</source>
        <translation type="unfinished">Ich kann dich nicht dorthin bringen!</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="423"/>
        <source>I don&apos;t do negatives...</source>
        <translation type="unfinished">Ich mache keine Negative ...</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="446"/>
        <source>Sorry, I can&apos;t take you there.</source>
        <translation type="unfinished">Entschuldigung, ich kann dich nicht dorthin bringen.</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="447"/>
        <source>Where do you want to go?</source>
        <translation type="unfinished">Wohin willst du gehen?</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="496"/>
        <source>Could you try rephrasing that?</source>
        <translation type="unfinished">Könnten Sie versuchen, das neu zu formulieren?</translation>
    </message>
    <message>
        <location filename="../o20.word/SpectrumGui.cpp" line="502"/>
        <source>
By the way, some of my features are disabled when editing plain text.</source>
        <translation type="unfinished">
Übrigens sind einige meiner Funktionen beim Bearbeiten von einfachem Text deaktiviert.</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Normal</source>
        <translation type="unfinished">Normal</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Heading 1</source>
        <translation type="unfinished">Überschrift 1</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Heading 2</source>
        <translation type="unfinished">Überschrift 2</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Heading 3</source>
        <translation type="unfinished">Überschrift 3</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Heading 4</source>
        <translation type="unfinished">Überschrift 4</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="31"/>
        <source>Heading 5</source>
        <translation type="unfinished">Überschrift 5</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Subtitle</source>
        <translation type="unfinished">Untertitel</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Emphasis</source>
        <translation type="unfinished">Betonung</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Strong</source>
        <translation type="unfinished">Fett gedruckt</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Quote</source>
        <translation type="unfinished">Zitat</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Reference</source>
        <translation type="unfinished">Referenz</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="32"/>
        <source>Book Title</source>
        <translation type="unfinished">Buchtitel</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="187"/>
        <source>Update style</source>
        <translation type="unfinished">Stil aktualisieren</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="188"/>
        <source>Update style to match selection</source>
        <translation type="unfinished">Aktualisieren Sie den Stil entsprechend der Auswahl</translation>
    </message>
    <message>
        <location filename="../o20.word/WordStyleInterface.cpp" line="209"/>
        <source>Restore default styles</source>
        <translation type="unfinished">Standardstile wiederherstellen</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="408"/>
        <source>
`Put your comment here`</source>
        <translation type="unfinished">
`Geben Sie hier Ihren Kommentar ein`</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="409"/>
        <source>: Put your comment here`</source>
        <translation type="unfinished">: Geben Sie hier Ihren Kommentar ein`</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="652"/>
        <location filename="../o20.word/TextGuiCommands.cpp" line="884"/>
        <source>Spectrum is AutoSaving this document.</source>
        <translation type="unfinished">Spectrum speichert dieses Dokument automatisch.</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="662"/>
        <source>This document is ReadOnly.</source>
        <translation type="unfinished">Dieses Dokument ist schreibgeschützt.</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="700"/>
        <source>Sorry, there was an error saving your document. ANY CHANGES MADE TO THIS DOCUMENT WILL BE LOST.</source>
        <translation type="unfinished">Es ist ein Fehler beim Speichern Ihres Dokuments aufgetreten. JEGLICHE ÄNDERUNGEN AN DIESEM DOKUMENT WERDEN VERLOREN.</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="771"/>
        <source>Unsupported format</source>
        <translation type="unfinished">Nicht unterstütztes Format</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="1011"/>
        <source>Your document has been AutoSaved to &lt;span style=&apos;font-family: Ubuntu&apos;&gt;%1&lt;/span&gt;.</source>
        <translation type="unfinished">Ihr Dokument wurde automatisch in &lt;span style=&apos;font-family: Ubuntu&apos;&gt;%1&lt;/span&gt; gespeichert.</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="773"/>
        <source>O20 only supports ODT and HTML, which most word processors (Office365, Office Online, Google Docs, and LibreOffice) support. Try converting this document into one of these formats.</source>
        <translation type="unfinished">O20 unterstützt nur ODT und HTML, die von den meisten Textverarbeitungsprogrammen (Office365, Office Online, Google Text &amp; Tabellen und LibreOffice) unterstützt werden. Versuchen Sie, dieses Dokument in eines dieser Formate zu konvertieren.</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="772"/>
        <source>The document %1 appears to be a Microsoft %2 document.</source>
        <translation type="unfinished">Das Dokument %1 scheint ein Microsoft %2-Dokument zu sein</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="927"/>
        <source>Remove from List</source>
        <translation type="unfinished">Aus Liste entfernen</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="1061"/>
        <source>Chapter</source>
        <translation type="unfinished">Kapitel</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="1062"/>
        <source>Introduction</source>
        <translation type="unfinished">Einführung</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="1062"/>
        <source>Conclusion</source>
        <translation type="unfinished">Fazit</translation>
    </message>
    <message>
        <location filename="../o20.word/TextGuiCommands.cpp" line="1095"/>
        <source>This document has no headings.</source>
        <translation type="unfinished">Dieses Dokument hat keine Überschriften.</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="27"/>
        <source>Unsaved document</source>
        <translation type="unfinished">Nicht gespeichertes Dokument</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="28"/>
        <source>Sure you want to exit?</source>
        <translation type="unfinished">Sicher möchten Sie beenden?</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="29"/>
        <source>This document hasn&apos;t been saved yet, so I couldn&apos;t AutoSave it.</source>
        <translation type="unfinished">Dieses Dokument wurde noch nicht gespeichert, daher konnte ich es nicht automatisch speichern.</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="30"/>
        <source>I haven&apos;t been AutoSaving this document. If you exit now, all changes will be lost.</source>
        <translation type="unfinished">Ich habe dieses Dokument nicht automatisch gespeichert. Wenn Sie jetzt beenden, gehen alle Änderungen verloren.</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="67"/>
        <source>  (ReadOnly)</source>
        <translation type="unfinished">  (Schreibgeschützt)</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="71"/>
        <source> (Code)</source>
        <translation type="unfinished">  (Code)</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="74"/>
        <source>  (AutoSave)</source>
        <translation type="unfinished">  (Automatisch speichern)</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="79"/>
        <source> - Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="81"/>
        <source> - Saved to your Linux PC</source>
        <translation type="unfinished"> - Auf Ihrem Linux-PC gespeichert</translation>
    </message>
    <message>
        <location filename="../o20.word/WordEvents.cpp" line="83"/>
        <source> - Modified</source>
        <translation type="unfinished"> - Geändert</translation>
    </message>
    <message>
        <location filename="../o20.word/WordOptions.cpp" line="73"/>
        <source>%1&lt;br/&gt;&lt;span style=&apos;font-size: 8pt; color=lightgray; white-space: pre;&apos;&gt;%2    %3&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../o20.word/WordOptions.cpp" line="73"/>
        <source>ddd MMMM d yyyy &apos;at&apos; hh:mm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
