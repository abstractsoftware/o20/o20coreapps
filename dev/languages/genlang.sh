FLIST="../o20.word/O20Gui.cpp ../o20.word/SpectrumGui.cpp ../o20.word/WordStyleInterface.cpp ../o20.word/WordOptions.cpp
       ../o20.word/TextGuiCommands.cpp ../o20.word/WordEvents.cpp ../o20.word/O20Ui.ui"
lupdate-qt5 $FLIST -ts o20_en.ts -target-language en
lupdate-qt5 $FLIST -ts o20_es.ts -target-language es
lupdate-qt5 $FLIST -ts o20_fr.ts -target-language fr
lupdate-qt5 $FLIST -ts o20_de.ts -target-language de
lupdate-qt5 $FLIST -ts o20_ru.ts -target-language ru
lupdate-qt5 $FLIST -ts o20_zh.ts -target-language zh
lupdate-qt5 $FLIST -ts o20_pt.ts -target-language pt
lupdate-qt5 $FLIST -ts o20_hi.ts -target-language hi
lupdate-qt5 $FLIST -ts o20_ja.ts -target-language ja
lupdate-qt5 $FLIST -ts o20_it.ts -target-language it
lupdate-qt5 $FLIST -ts o20_cs.ts -target-language cs
lupdate-qt5 $FLIST -ts o20_el.ts -target-language el
