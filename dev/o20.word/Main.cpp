/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QScreen>
#include <QFile>
#include <QStyleFactory>
#include <QGraphicsDropShadowEffect>

#include <QQmlEngine>
#include <QQuickStyle>
#include <QQuickView>
#include <QQuickItem>
#include <QTest>

#include "O20Gui.h"

#include <O20>
#include <O20Application.h>
#include <O20AsciiArt.h>

#include <cstdio>

int main(int argc, char *argv[])
{
    O20::generateStartupArt();
    O20::ApplicationMetadata metaData;
    return O20::O20_OfficeApp(argc, argv, metaData);
}
