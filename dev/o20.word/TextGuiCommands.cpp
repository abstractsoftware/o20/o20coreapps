#include "O20Gui.h"
#include "src/ui_O20Ui.h"

#include <converter.h>

#include <QTest>
#include <QQuickItem>
#include <QtWidgets>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

void O20Gui::setTextSpacing() {
    qreal height = 1;
    if (sender() == spacingList[0]) height = 100;
    else if (sender() == spacingList[1]) height = 115;
    else if (sender() == spacingList[2]) height = 150;
    else if (sender() == spacingList[3]) height = 200;
    else if (sender() == spacingList[4]) height = 250;

    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setLineHeight(height, QTextBlockFormat::ProportionalHeight);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
}

void O20Gui::setTextUnderlineStyle() {
    QTextCharFormat::UnderlineStyle style = QTextCharFormat::SingleUnderline; // default
    if (sender() == underlineList[0]) style = QTextCharFormat::NoUnderline;
    else if (sender() == underlineList[2]) style = QTextCharFormat::DashUnderline;
    else if (sender() == underlineList[3]) style = QTextCharFormat::DotLine;
    else if (sender() == underlineList[4]) style = QTextCharFormat::DashDotLine;
    else if (sender() == underlineList[5]) style = QTextCharFormat::DashDotDotLine;
    else if (sender() == underlineList[6]) style = QTextCharFormat::WaveUnderline;

    if (!(ui->setUnderline->isChecked() or sender() != ui->setUnderline)) style = QTextCharFormat::NoUnderline;

    QTextCursor cursor = ui->docView->textCursor();
    QTextCharFormat charFormat = cursor.charFormat();
    charFormat.setUnderlineStyle( style );
    cursor.setCharFormat(charFormat);
    ui->setUnderline->setChecked(true);
}

void O20Gui::setTextListOn() {
    QTextListFormat::Style style = QTextListFormat::ListDisc; // default
    if (sender() == bulletList[0]) style = QTextListFormat::ListDisc;
    else if (sender() == bulletList[1]) style = QTextListFormat::ListCircle;
    else if (sender() == bulletList[2]) style = QTextListFormat::ListSquare;
    else if (sender() == bulletList[3]) style = QTextListFormat::ListDecimal;
    else if (sender() == bulletList[4]) style = QTextListFormat::ListLowerAlpha;
    else if (sender() == bulletList[5]) style = QTextListFormat::ListUpperAlpha;
    else if (sender() == bulletList[6]) style = QTextListFormat::ListLowerRoman;
    else if (sender() == bulletList[7]) style = QTextListFormat::ListUpperRoman;

    if (ui->bulletedlist->isChecked() or sender() != ui->bulletedlist) {
        QTextCursor cursor = ui->docView->textCursor();
        QTextListFormat listFormat;
        listFormat.setStyle( style );
        cursor.createList( listFormat );
        ui->bulletedlist->setChecked(true);
    } else {
        RemoveList();
    }
}

void O20Gui::RemoveList()
{
    QTextCursor cursor = ui->docView->textCursor();
    QTextList *list = cursor.currentList();
    if( list ) {
        QTextListFormat listFormat;
        listFormat.setIndent( 0 );
        //listFormat.setStyle( QTextList );
        list->setFormat( listFormat );
        for( int i = list->count() - 1; i >= 0 ; --i )
            list->removeItem( i );
     }
}

void O20Gui::startCoreConnections()
{
    connect(ui->ribbon, SIGNAL(currentChanged(int)), SLOT(SwitchScreens())); // tabBarDoubleClicked(int)
    connect(ui->gobackremode, SIGNAL(released()), SLOT(SwitchScreens()));
    connect(ui->goback, SIGNAL(released()), SLOT(SwitchScreens()));
    connect(ui->backtoedit, SIGNAL(released()), SLOT(SwitchScreens()));
    //connect(ui->startsave, SIGNAL(released()), ui->save, SLOT(click()));


    ui->homeScrollFwd->hide();
    ui->homeScrollBk->hide();
    wasPageBefore = true;
    connect(ui->homeScroll->horizontalScrollBar(), &QScrollBar::rangeChanged, [=]() {
		    if (ui->homeScroll->horizontalScrollBar()->maximum() == 0) {
                        ui->homeScrollFwd->hide(); ui->homeScrollBk->hide();
		    } else {
                        ui->homeScrollFwd->show(); ui->homeScrollBk->show();
		    }

		    // Automatically change to web view when the windows becomes too small??
		    // Not sure about is :/ -> TODO maybe later
		    /*if (ui->homeScroll->horizontalScrollBar()->maximum() == 0) {
                        if (wasPageBefore) ui->pageview->click(), wasPageBefore = true;
                    } else {
                        if (!ui->webview->isChecked()) {
                            wasPageBefore = true;
			    ui->webview->click();
                        }
                    }*/
    });

    connect(ui->homeScrollFwd, &QToolButton::released, [=]() {
        ui->homeScroll->horizontalScrollBar()->setValue(ui->homeScroll->horizontalScrollBar()->value() + ui->homeScroll->horizontalScrollBar()->pageStep());
    });

    connect(ui->homeScrollBk, &QToolButton::released, [=]() {
        ui->homeScroll->horizontalScrollBar()->setValue(ui->homeScroll->horizontalScrollBar()->value() - ui->homeScroll->horizontalScrollBar()->pageStep());
    });

    connect(ui->shrinkSidebar, &QToolButton::released, [=]() {
        if (isSidebarMinimized) {
            QPropertyAnimation *animation = new QPropertyAnimation(ui->goback, "minimumSize");
            animation->setDuration(100);
            animation->setStartValue(QSize(51, 0));
            animation->setEndValue(QSize(100, 0));

            animation->start();

            connect(animation, &QPropertyAnimation::finished, [=](){ 
                ui->goback->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Home->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->New->setStyleSheet("margin-bottom: 0px;  min-width: 100px; padding-left: 10px;");
                ui->Open->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Save->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");
                ui->Options->setStyleSheet("padding-left: 10px; min-width: 100px;");
                ui->About->setStyleSheet("margin-bottom: 0px; min-width: 100px; padding-left: 10px;");

                ui->shrinkSidebar->setIcon(QIcon(":/BreezeDark/go-previous-skip.svg"));
                ui->goback->setToolButtonStyle(Qt::ToolButtonTextBesideIcon); 
                ui->Home->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->New->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Open->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Save->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
		ui->About->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                ui->Options->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
            });
        } else {
            ui->goback->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Home->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->New->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Open->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Save->setToolButtonStyle(Qt::ToolButtonIconOnly);
	    ui->About->setToolButtonStyle(Qt::ToolButtonIconOnly);
            ui->Options->setToolButtonStyle(Qt::ToolButtonIconOnly);


            QPropertyAnimation *animation = new QPropertyAnimation(ui->goback, "minimumSize");
            animation->setDuration(100);
            animation->setStartValue(QSize(100, 0));
            animation->setEndValue(QSize(51, 0));

            ui->goback->setStyleSheet("margin-bottom: 0px;");
            ui->Home->setStyleSheet("margin-bottom: 0px;");
            ui->New->setStyleSheet("margin-bottom: 0px;;");
            ui->Open->setStyleSheet("margin-bottom: 0px;");
            ui->Save->setStyleSheet("margin-bottom: 0px;");
	    ui->About->setStyleSheet("margin-bottom: 0px;");
            ui->Options->setStyleSheet("");


            animation->start();

            connect(animation, &QPropertyAnimation::finished, [=](){
                ui->shrinkSidebar->setIcon(QIcon(":/BreezeDark/go-next-skip.svg"));
            });
        }

        isSidebarMinimized = !isSidebarMinimized;
    });

    connect(ui->blankfromtemplate, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));


    connect(ui->newdoc2, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->singlespace, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->genotes, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->createsimple, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->buisnessletter, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->boldreport, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->studentreport, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->resumecover, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));
    connect(ui->resume, SIGNAL(released()), SLOT(CreateBlankFromTemplate()));

    connect(ui->openfilelocation, SIGNAL(released()), SLOT(OpenFileLocation()));

    /*connect(ui->indentmore, &QToolButton::released, [=](){
    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setTextIndent(fmt.textIndent()+1);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
    });

    connect(ui->indentless, &QToolButton::released, [=](){
    QTextCursor cursor = ui->docView->textCursor();
    QTextBlockFormat fmt = cursor.blockFormat();
    fmt.setTextIndent(fmt.textIndent()-1);
    cursor.setBlockFormat(fmt);
    ui->docView->setTextCursor(cursor);
    }); */

    connect(ui->insertChar, &QToolButton::released, [=]() { charSelectDialog->show(); });

    //connect(ui->closesidebar, SIGNAL(released()), SLOT(CloseSidebar()));
    connect(ui->spectrumintro, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->whatsnew, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->helphome, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->navButton, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->closeEntireSidebar, SIGNAL(released()), SLOT(CloseSidebar()));
    //connect(ui->ribbonhelp, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribboninfo, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonlicense, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonstar, SIGNAL(released()), SLOT(HelpIndex()));
    //connect(ui->ribbonspectrum, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->aboutword, SIGNAL(released()), SLOT(HelpIndex()));
    connect(ui->office20license, SIGNAL(released()), SLOT(HelpIndex()));

   connect(ui->insertImage, &QToolButton::released, [=]() {
           QFileDialog qf;
           qf.selectFile(defaultname);
           qf.setAcceptMode(QFileDialog::AcceptOpen);
           qf.setNameFilter("Support Image Formats (*.bmp *.jpg *.jpeg *.gif *.png *.svg);; All Files (*)");
           if (qf.exec() == QDialog::Rejected) return;
           QString file = qf.selectedFiles()[0];
           QUrl Uri (QString ( "file://%1" ).arg ( file ));

           QImage image = QImageReader ( file ).read();
           //if (image.width() > 600) image = image.scaled(600, 600, Qt::KeepAspectRatio);

           QTextDocument * textDocument = ui->docView->document();
           textDocument->addResource(QTextDocument::ImageResource, Uri, QVariant ( image ));

           QTextCursor cursor = ui->docView->textCursor();
           QTextImageFormat imageFormat;
           imageFormat.setWidth( image.width() );
           imageFormat.setHeight( image.height() );
           imageFormat.setName( Uri.toString() );
           cursor.insertImage(imageFormat);
           ui->docView->setTextCursor(cursor);
           });

   connect(ui->setOverline, &QToolButton::released, [=]() {
           QTextCursor cursor = ui->docView->textCursor();
           QTextCharFormat fmt = cursor.blockCharFormat();
           fmt.setFontOverline(ui->setOverline->isChecked());
           cursor.setCharFormat(fmt);
           ui->docView->setTextCursor(cursor); 
           });

   connect(ui->insertTable, &QToolButton::released, [=]() {
           QTextTableFormat fmt;
           fmt.setCellSpacing(0);
           fmt.setCellPadding(5);
	   fmt.setBorder(0.5);
	   //fmt.setBorderBrush(Qt::cyan);
           fmt.setWidth(QTextLength(QTextLength::PercentageLength, 100));

           QTextCursor cursor = ui->docView->textCursor();
           QTextTable *table = cursor.insertTable(tablew->value(), tableh->value(), fmt);
           });

   connect(ui->insertLink, &QToolButton::released, [=]() {
           QString linkname = urlname->text();
           if (linkname == "") linkname = url->text();

           QTextCursor cursor = ui->docView->textCursor(); cursor.insertHtml("<a href='" + url->text() + "'>" + linkname + "</a>");
           });

    connect(ui->recentFiles, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(ioReceiver()));

    connect(ui->doSpellCheck, &QToolButton::released, [=]() {
            if (ui->doSpellCheck->isChecked()) CreateSpellCheckerObject();
	    else DeleteSpellCheckerObject();
    });

    connect(ui->checkspell, &QToolButton::released, ui->docView, &KTextEdit::checkSpelling);
    connect(ui->doreplace, &QToolButton::released, ui->docView, &KTextEdit::replace);

    connect(ui->insertPageBreak, &QToolButton::released, [=]() {
            QTextCursor cursor = ui->docView->textCursor();
	    ui->docView->insertPlainText("\n");
	    QTextBlockFormat blockFormat = cursor.blockFormat();
            blockFormat.setPageBreakPolicy(QTextFormat::PageBreak_AlwaysBefore);
            cursor.setBlockFormat(blockFormat);
	    ui->docView->insertPlainText("\n");
            blockFormat = cursor.blockFormat();
            blockFormat.setPageBreakPolicy(QTextFormat::PageBreak_Auto);
            cursor.setBlockFormat(blockFormat);
	    });

    connect(ui->ribbon, &QTabWidget::tabBarDoubleClicked, [=]() {
		    if (tabshidden && ui->ribbon->currentIndex() != 0) ShowTabs();
		    else HideTabs();
    });

    connect(ui->setStrikeout,   &QToolButton::released, [=]() { ui->docView->setTextStrikeOut(ui->setStrikeout->isChecked()); });
    connect(ui->setSuperscript, &QToolButton::released, [=]() { ui->docView->setTextSuperScript(ui->setSuperscript->isChecked()); });
    connect(ui->setSubscript,   &QToolButton::released, [=]() { ui->docView->setTextSubScript(ui->setSubscript->isChecked()); });
    connect(ui->setBold,   &QToolButton::released, [=]() { ui->docView->setTextBold(ui->setBold->isChecked()); });
    connect(ui->setItalic, &QToolButton::released, [=]() { ui->docView->setTextItalic(ui->setItalic->isChecked()); });
    //connect(ui->setUnderline, &QToolButton::released, [=]() { ui->docView->setTextUnderline(ui->setUnderline->isChecked()); });

    connect(ui->headingList, &QListWidget::itemClicked, [=]() {

                  if (headingPosList.size() == 0) return;
		  if (!isReadingMode) {
                      QTextCursor textCursor = ui->docView->textCursor();
                      textCursor.setPosition (headingPosList[ui->headingList->currentRow()]);
                      ui->docView->setFocus();
                      ui->docView->setTextCursor (textCursor);
		  } else {
                      QTextCursor textCursor = ui->readingBrowser->textCursor();
                      textCursor.setPosition (headingPosList[ui->headingList->currentRow()]);
                      ui->readingBrowser->setFocus();
                      ui->readingBrowser->setTextCursor (textCursor);
		  }
     });

    connect(ui->headingList_2, &QListWidget::itemClicked, [=]() {

                  if (commentPosList.size() == 0) return;
                  if (!isReadingMode) {
                      QTextCursor textCursor = ui->docView->textCursor();
                      textCursor.setPosition (commentPosList[ui->headingList_2->currentRow()]);
                      ui->docView->setFocus();
                      ui->docView->setTextCursor (textCursor);
                  } else {
                      QTextCursor textCursor = ui->readingBrowser->textCursor();
                      textCursor.setPosition (commentPosList[ui->headingList_2->currentRow()]);
                      ui->readingBrowser->setFocus();
                      ui->readingBrowser->setTextCursor (textCursor);
                  }
     });

    connect(ui->setColor,   SIGNAL(released()), SLOT(EditingCommands()));
    connect(ui->setBgColor, SIGNAL(released()), SLOT(EditingCommands()));

    //  QDesktopServices::openUrl(QUrl("file:///C:/Documents and Settings/All Users/Desktop", QUrl::TolerantMode));

    connect(ui->aboutQt, &QToolButton::released, [=]() { QMessageBox::aboutQt(this);  });
    //connect(ui->o20Website, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://abstractsoftware.gitlab.io/o20/site"));  });
    connect(ui->reportBug, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://gitlab.com/abstractsoftware/o20/o20coreapps/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D="));  });
    //connect(ui->o20Flathub, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://flathub.org/apps/details/io.gitlab.o20.word"));  });
    //connect(ui->o20snapstore, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://snapcraft.io/office20"));  });
    connect(ui->abstractsoftware, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://abstractsoftware.gitlab.io"));  });
    connect(ui->submitIdea, &QToolButton::released, [=]() { QDesktopServices::openUrl(QUrl("https://gitlab.com/abstractsoftware/o20/o20coreapps/issues/7"));  });

    connect(ui->fontSizeComboBox,  SIGNAL(currentTextChanged(QString)), SLOT(EditingCommands()));
    connect(ui->fontComboBox,      SIGNAL(currentTextChanged(QString)), SLOT(EditingCommands()));
    connect(ui->eraseFormat,       SIGNAL(released()), SLOT(EditingCommands()));
    connect(ui->alignleft,   SIGNAL(released()), ui->docView, SLOT(alignLeft()));
    connect(ui->alignright,  SIGNAL(released()), ui->docView, SLOT(alignRight()));
    connect(ui->aligncenter, SIGNAL(released()), ui->docView, SLOT(alignCenter()));
    connect(ui->alignjustify,SIGNAL(released()), ui->docView, SLOT(alignJustify()));

    //connect(ui->upload,     SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->openButton, SIGNAL(released()), SLOT(ioReceiver()));
    //connect(ui->openODT, SIGNAL(released()), SLOT(ioReceiver()));
    //connect(ui->openXHTML, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->openTextFileButton, SIGNAL(released()), SLOT(ioReceiver()));
    //connect(ui->browseDownloads, SIGNAL(released()), SLOT(ioReceiver()));
    //connect(ui->browseDocuments, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->openstart,  SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->save,      SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->saveButton, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->saveAs,    SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->saveAsPDF, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->print, SIGNAL(released()), SLOT(ioReceiver()));


    connect(ui->autohead->rootObject()->findChild<QObject*>("Switch"), SIGNAL(released()), SLOT(updateNavigation()));
    connect(ui->docView,  SIGNAL(textChanged()), SLOT(slotSetTitle()));
    connect(ui->docView,  SIGNAL(textChanged()), SLOT(onTextChanged()));
    connect(ui->docView,  SIGNAL(cursorPositionChanged()), SLOT(onCursorUpdate()));
    connect(ui->docView->document(), SIGNAL(modificationChanged(bool)), SLOT(slotSetTitle()));

    connect(ui->Home, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->New,  SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->Open, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->Options, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->Save, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));
    connect(ui->About, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));

    connect(ui->startHome, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startNew,  SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startOpen, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startOptions, SIGNAL(released()), SLOT(SidebarSwitcher()));
    connect(ui->startAbout, SIGNAL(toggled(bool)), SLOT(SidebarSwitcher()));

    //ui->scrollview->hide();

    connect(ui->noUserPhoto, &QCheckBox::released, [=]() {
		    ui->photowid->setHidden(!ui->noUserPhoto->isChecked());
		    });

    connect(ui->insertComment, &QToolButton::released, [=]() {
		QTextCursor cc = ui->docView->textCursor();
                if (uname.isEmpty()) cc.insertText(tr("\n`Put your comment here`"));
		else cc.insertText("\n`" + uname + tr(": Put your comment here`"));
                ui->docView->setTextCursor(cc);
    });

    connect(ui->exportHtml, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->exportText, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->renameDocument, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->deleteDocument, SIGNAL(released()), SLOT(ioReceiver()));

    connect(ui->openDocument, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->openDocumentLocked, SIGNAL(released()), SLOT(ioReceiver()));
    connect(ui->openDocumentLocation, &QToolButton::released, [=]() {
		    if (ui->recentFiles->selectedItems().size() == 0) return;
		    QDesktopServices::openUrl(QUrl::fromLocalFile(QFileInfo(recentfilenames.at(ui->recentFiles->currentRow())).path()));
		    });
    connect(ui->removeDocument, &QToolButton::released, [=]() {
		    if (ui->recentFiles->selectedItems().size() == 0) return;
		    recentfilenames.removeAt(ui->recentFiles->currentRow());
		    UpdateRecentFiles();
		    UpdateRecentFilesWidget();
		    });

    /*
    connect(ui->updateCurrentStyle, &QToolButton::released, [=]() {
		    QString currentstylename = "Normal";
		    for (QString stylename : stylenames) {
                        if (styleCharFormats[stylename] == ui->docView->currentCharFormat()) currentstylename = stylename;
                    }

                    bool ok;
                    QFont font =  QFontDialog::getFont(&ok, styleCharFormats[currentstylename].font(), this);

                    if (ok) {
			QTextCharFormat currentFormat = styleCharFormats[currentstylename];
                        styleCharFormats[currentstylename].setFont(font);

			QTextCursor c = ui->docView->textCursor();
			int pos = c.position();
			c.clearSelection();
                        for (QTextBlock it = ui->docView->document()->begin(); it != ui->docView->document()->end(); it = it.next()) {
                            c.setPosition(it.position());
                            if (c.charFormat().font() == currentFormat.font()) {
			        c.setPosition(it.position());
				c.select(QTextCursor::BlockUnderCursor);
				c.setCharFormat(styleCharFormats[currentstylename]);
			    }
                        }

			c.clearSelection();
			c.setPosition(pos);
		    }
     });*/

    connect(ui->pageview, SIGNAL(released()), SLOT(SetView()));
    connect(ui->webview,  SIGNAL(released()), SLOT(SetView()));
    //connect(ui->scrollview,  SIGNAL(released()), SLOT(SetView()));
    connect(ui->readingMode, SIGNAL(released()), SLOT(SetView()));
    connect(ui->closeReadingMode, &QToolButton::released, [=](){
            if (readOnly) return;
            ui->pageStack->setCurrentIndex(0);

	    ui->quickDock->hide();
            reader.stop();

            if (ribbonShownBefore) ui->ribbon->show();
	    if (statusBarShownBefore) ui->statusbar->show();
            ui->windec->show();

	    if (isFullScreen()) {
	        showNormal();
	        if (wasMaximized)
                      setWindowState(windowState() ^ Qt::WindowMaximized);
	        else
                    showNormal();
            }

            if (ui->docView->usePageMode()) {
                ///if (ui->docView->isPageContinuous()) ui->scrollview->setChecked(true);
                /*else*/ ui->pageview->setChecked(true);
            } else {
                ui->webview->setChecked(true);
            }

	    isReadingMode = false;
    });

    /*connect(ui->readAloud, &QToolButton::released, [=]() {
		            reader.stop();
			    QString speechsay = ui->readingBrowser->textCursor().selectedText();
			    if (speechsay.isEmpty() or speechsay.isNull()) speechsay = ui->readingBrowser->toPlainText();
			    //speechsay.replace("\n", ". \n");
			    //speechsay.replace("..", ". ");
			    //speechsay.replace(". .", ". ");
			    //speechsay.replace(",.", ", ");
		            reader.say(speechsay);
		    });
    connect(ui->readStop, &QToolButton::released, [=]() { reader.stop(); });*/

    connect(ui->showInFullscreen, &QToolButton::released, [=]() {
        if (isFullScreen()) {
                //showNormal();
                if (wasMaximized)
                      setWindowState(Qt::WindowMaximized); //setWindowState(windowState() ^ Qt::WindowMaximized);
                else
                    showNormal();
	} else wasMaximized = isMaximized(), showFullScreen();
   });

    //connect(ui->audioMode,   SIGNAL(released()), SLOT(SetView()));


    //connect(ui->enableSpectrum, &QToolButton::released, [=]() { ui->spectrumSearch->setHidden(!ui->enableSpectrum->isChecked()); });
}

bool O20Gui::ioReceiver(QString mode, QString where, bool lock)
{
    if (nostart) {
        ShowScreen("EDIT");
        ui->ribbon->setCurrentIndex(1);
    }

    if (sender() == ui->openButton or sender() == ui->openstart or //sender() == ui->openODT or sender() == ui->openXHTML or
		    sender() == ui->openTextFileButton or sender() == ui->recentFiles or sender() == ui->openDocument or sender() == ui->openDocumentLocked or mode == "open:") {


        QFileDialog qf;
        qf.selectFile(defaultname);
        qf.setAcceptMode(QFileDialog::AcceptOpen);
        qf.setNameFilter("Deduct from Extension (*);;"
            "HTML Files (*.html *.xhtml *.htm);;"
            "Open Document Text (*.odt *.ott *.odf);;"
            "Code and Text (*)");

       /* precise filter for convenience! */
       if (sender() == ui->openTextFileButton) qf.setNameFilter("Plain Text (*)");
       //else if (sender() == ui->openODT) qf.setNameFilter("Open Document Text (*.odt *.ott *.odf)");
       //else if (sender() == ui->openXHTML) qf.setNameFilter("HTML Files (*.xhtml *.html *.htm)");

        QString name = "";
        bool lockthefile = ui->openLocked->isChecked();
        if (sender() != ui->recentFiles and sender() != ui->openDocument and sender() != ui->openDocumentLocked and mode != "open:")
            if (qf.exec() == QDialog::Rejected) return false;
            else name = qf.selectedFiles()[0];
        else if (mode == "open:") name = where, lockthefile = lock; // NULL, NONE, NAN
	else {
           if (ui->recentFiles->selectedItems().size() == 0) return false;
           name = recentfilenames.at(ui->recentFiles->currentRow());
           if (sender() == ui->openDocumentLocked) lockthefile = true;
        }

        if (!nostart or (ui->docView->toPlainText() == "" && document.filename == "")) {
           if (!openDocument(name, lockthefile, lockthefile)) {
		   return false;
	   }

	} else {
           O20Gui* w = new O20Gui();
           w->ioReceiver("open:", name, lockthefile);
           w->startCore();
           w->showApp();
        }

        ui->docView->document()->setModified(false);
	nostart = true;
    } else if (sender() == ui->save or sender() == ui->saveButton or sender() == ui->saveAs or sender() == ui->saveAsPDF
		    or sender() == ui->exportHtml or sender() == ui->exportText) {
        QString filename, suffix;
	bool temp = false;
	if (sender() == ui->saveAs or sender() == ui->saveAsPDF) temp = true;

        if (document.filename == "" or temp) {
            QFileDialog qf;
            qf.selectFile(defaultname);
            qf.setAcceptMode(QFileDialog::AcceptSave);
            qf.setNameFilter("Supported Formats (*.odt *.ott *.xhtml *.html);; Open Document Text (*.odt *.ott);;"
                             "HTML Files (*.xhtml *.html);; All Files (*)");

	    if (sender() == ui->saveAsPDF) qf.setNameFilter("PDF Files (*.pdf)");
	    else if (sender() == ui->exportHtml) qf.setNameFilter("HTML Files (*.xhtml *.html)");
	    else if (sender() == ui->exportText) qf.setNameFilter("All Files (*)");
            if (qf.exec() == QDialog::Rejected)
                return false;

            filename = qf.selectedFiles()[0];
            suffix = QFileInfo(filename).suffix();
        }

        bool sucess;
        QTextDocumentWriter writer(filename);
        if (suffix == "odt" or suffix == "ott") {
            writer.setFormat("ODT");
        } else if (suffix == "html" or suffix == "xhtml") {
	    writer.setFormat("HTML");
	} else if (suffix == "pdf") {
            bool wasOn = spellCheckOn;
            DeleteSpellCheckerObject();
    	    QPrinter printer(QPrinter::PrinterResolution);
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOutputFileName(filename);
            printer.setPageSize(QPrinter::Letter);
            printer.setPageMargins(.1, .1, .1, .1, QPrinter::Inch);
            QSizeF pageSizeBefore = ui->docView->document()->pageSize();
            ui->docView->document()->setPageSize(printer.pageRect().size());
            ui->docView->document()->print(&printer);
            ui->docView->document()->setPageSize(pageSizeBefore);
	    if (wasOn) CreateSpellCheckerObject();
	    goto nowrite;
	} else {
	    writer.setFormat("PLAINTEXT");
        }

        sucess = writer.write(ui->docView->document());
	if (!temp && document.filename == "") {
            document.filename = filename;
	    document.suffix = suffix;
	}

        ui->docView->document()->setModified(false);
        setWindowTitle();
        nowrite: ;
    } else if (sender() == ui->print) {
        DeleteSpellCheckerObject();

	QPrinter printer(QPrinter::PrinterResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        QPrintDialog printDialog(&printer);
        if (printDialog.exec() == QDialog::Accepted) {
            printer.setPageSize(QPrinter::A3);
            printer.setPageMargins(1, 1, 1, 1, QPrinter::Inch);
            QSizeF pageSizeBefore = ui->docView->document()->pageSize();
            ui->docView->document()->setPageSize(printer.pageRect().size());
            ui->docView->document()->print(&printer);
            ui->docView->document()->setPageSize(pageSizeBefore);
        }

	CreateSpellCheckerObject();
    } else if (sender() == ui->renameDocument) {
         QFileDialog qf;
         qf.selectFile(defaultname);
         qf.setAcceptMode(QFileDialog::AcceptSave);

         if (document.suffix == "odt") qf.setNameFilter("Open Document Text (*.odt *.ott)");
	 else if (document.suffix == "html") qf.setNameFilter("HTML Files (*.xhtml *.html)");
	 else qf.setNameFilter("All Files (*)");

         if (qf.exec() == QDialog::Rejected)
             return false;

         QString filename = qf.selectedFiles()[0];
         QString suffix = QFileInfo(filename).suffix();

	 bool a = QFile(document.filename).rename(filename);

	 //if (!a) QFile(filename).remove(), QFile(document.filename).rename(filename);

	 document.filename = filename;
	 document.suffix = "odt";
    } else if (sender() == ui->deleteDocument) {
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Dangerous Action"));
            msgBox.setText(QString(QString(tr("Are you sure your want to delete %1?")).arg(document.filename)));
            msgBox.setInformativeText(tr("If you press Yes, this document will be PERMANENTLY DELETED!!"));
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::No);
            msgBox.setIcon(QMessageBox::Warning);
            int a = msgBox.exec();

	    if (a == QMessageBox::No) return false;
	    else {
                QFile(document.filename).remove();
		document.filename = "";
		wasDeleted = true;
                close();
	    }
    }

    if (nostart) {
        if (mode == "") ShowScreen("EDITNOANIM");
	else ShowScreen("EDIT");
	enableEditing();
        ui->ribbon->setCurrentIndex(1);
    }

    if (document.filename != "") {
        //ui->startsave->hide();
	ShowSwitch();
        ui->save->hide();
        if (!readOnly && ui->notificationerror->isHidden()) {
            /// ui->autosavealert->show();
	    ui->autosavelabel->setText(tr("Spectrum is AutoSaving this document."));
	    ui->notification->show();
	}
        ui->openfilelocation->show();
    }

    ui->docView->document()->setModified(false);

    if (readOnly) {
        //ui->readonlyalert->show();
        ui->autosavelabel->setText(tr("This document is ReadOnly."));
        ui->notification->show();
        ui->readingMode->click();
    }

    return true;
    //setWindowTitle();
}

#include <odtpatch.h>

bool O20Gui::SaveDocument()
{
    if (readOnly) return false;

    //if (!locksave) locksave = true;
    //else return false;

/*    if (document.suffix == "docx" or document.suffix == "dotx") {
        QProcess docx;
        docx.start("pandoc -f html -t docx -o " + document.filename);
        docx.waitForStarted();
        docx.write(ui->docView->toHtml().toUtf8());
        docx.closeWriteChannel();
        docx.waitForFinished();
    } else*/ 

     QTextDocumentWriter writer(document.filename);
     if (document.suffix == "odt" or document.suffix == "ott")
         writer.setFormat("ODT");
     else if ((document.suffix == "html" or document.suffix == "xhtml") and !ui->openAsText->isChecked())
        writer.setFormat("HTML");
//#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
//     else if (document.suffix == "md" and !ui->openAsText->isChecked())
//         writer.setFormat("markdown");
//#endif
     else
        writer.setFormat("plaintext");

    bool a = writer.write(ui->docView->document());

    if (!a) {
        ui->autosavelabel_2->setText(tr("Sorry, there was an error saving your document. ANY CHANGES MADE TO THIS DOCUMENT WILL BE LOST."));
	ui->fixError->setIcon(QIcon(":/AxIcons/button-error_small@1x.png"));
        ui->notificationerror->show();
	ui->notification->hide();
	return false;
    } else {
        if (document.suffix == "odt" or document.suffix == "ott") O20::PatchODT(document.filename, stylenames, styleCharFormats, styleBlockFormats);
    }

    ui->docView->document()->setModified(false);
    nostart = true;

    locksave = false;
    setWindowTitle();
    AddRecentFile();
    return true;
}

#include <definition.h>
#include <msformats.h>

#define QTDOCUMENTSLOCATION QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
#define QTHOMELOCATION      QStandardPaths::writableLocation(QStandardPaths::HomeLocation)

QString O20Gui::generateNewDocPath() {
   QString _filename = "";
   QString _suffix = "odt";

   if (!askSaveLocation) {
        QString docs = qEnvironmentVariableIsSet("SNAP") ? QTHOMELOCATION : QTDOCUMENTSLOCATION;

        if(!QDir(docs + "/O20/Docs").exists()) {
            QDir().mkdir(docs + "/O20");
            QDir().mkdir(docs + "/O20/Docs");
        }

       int filenameIndex = 0;
        while (true) {
            filenameIndex++;
            _filename = docs + "/O20/Docs/Document" + QString::number(filenameIndex) + ".odt";
            if (!QFile::exists(_filename)) break;
        }
    } else {
        QFileDialog qf;
        qf.selectFile(defaultname);
        qf.setAcceptMode(QFileDialog::AcceptSave);
        qf.setNameFilter("Supported Formats (*.html *.htm *.odt *.ott);; Open Document Text (*.odt *.ott);;"
                         "HTML Files (*.xhtml *.html *.htm);; All Files (*)");

        if (qf.exec() == QDialog::Rejected)
            qApp->exit(); //false

        _filename = qf.selectedFiles()[0];
        _suffix = QFileInfo(_filename).suffix();
    }

   return _filename;
}

bool O20Gui::openDocument(QString name, bool readonly, bool noEdit)
{
    if (name == "" or !QFileInfo(name).exists()) {
        int a = QMessageBox::critical(this, tr("Nonexistent Document"), QString(tr("We could not open the document %1. Would you like to create it?")).arg(name), QMessageBox::No | QMessageBox::Yes, QMessageBox::No);

	if (a == QMessageBox::No)
            return false;
	else
            CreateBlankFromTemplate("", name);
    }


    QString suffix = QFileInfo(name).suffix();

    bool wasDocx = false;
    if (suffix == "docx" or suffix == "dotx" or suffix == "doc" or suffix == "dot") {
        if (suffix == "docx" or suffix == "dotx") wasDocx = true;

	if (!ui->tryExtractDocx->isChecked() or !wasDocx) { // can't and never will open DOC (binary)
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Unsupported format"));
            msgBox.setText(QString(tr("The document %1 appears to be a Microsoft %2 document.")).arg(name).arg(suffix.toUpper()));
            msgBox.setInformativeText(tr("O20 only supports ODT and HTML, which most word processors (Office365, Office Online, Google Docs, and LibreOffice) support. Try converting this document into one of these formats."));
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setIconPixmap(QPixmap(":/icons/rebrand/ms-word.svg"));
            msgBox.exec();
	    return false;
	}
    }

    if (readonly && noEdit) readOnly = true; 


    //ui->docView->setReadOnly(noEdit);

    if (wasDocx and ui->tryExtractDocx->isChecked()) {
        QString io = O20::MSFormats::convertMSDocument(name);

        if (io.isNull()) return false;
		
        ui->docView->setHtml(io);

	if (!readOnly) {
            name = generateNewDocPath(); //QFileInfo(name).canonicalPath() + "/" + QFileInfo(name).baseName();

            /*int filenameIndex = 0;
            QString _filename;

            while (true) {
                 filenameIndex++;
                 if (filenameIndex == 1) _filename = name + ".odt";
                 else _filename = name + "." + QString::number(filenameIndex) + ".odt";

                 if (!QFile::exists(_filename)) break;
	    }
	    name = _filename;
	    
	    */

            suffix = QFileInfo(name).suffix();
	}

    } else if (suffix == "odt" or suffix == "ott") {
        OOO::Converter odt;
        ui->pageview->setChecked(true);
        ui->docView->setDocument(odt.convert(name));
	OOO::StyleInformation* oooStyles = odt.getStyleInformation();
	GenerateAutoStyles(oooStyles);
        CreateSpellCheckerObject();

        ui->docView->setUsePageMode(true);
        ui->pageview->setChecked(true);
        if (ui->ribbon->isHidden()) ui->docView->setUsePageMode(false);
    } else {
	ui->docView->setUsePageMode(true);
	ui->pageview->setChecked(true);
        QFile import(name);
        import.open(QIODevice::ReadOnly | QIODevice::Text);
        QString io = import.readAll();
        import.close();

        if ((suffix == "html" or suffix == "xhtml") and !ui->openAsText->isChecked()) {
            ui->docView->setHtml(io);
//#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
//        } else if (suffix == "md" and !ui->openAsText->isChecked()) {
//            ui->docView->setMarkdown(io);
//#endif
        } else {
           ui->docView->setUsePageMode(false);
           ui->webview->setChecked(true);

	   DeleteSpellCheckerObject();
           const auto def = m_repository.definitionForFileName(name);
	   // TODO add fullscreen mode
	   if (darkModeCode) ui->docView->setBackgroundColor("#232629", "#ffffff");

           m_highlighter = new KSyntaxHighlighting::SyntaxHighlighter(ui->docView);
           m_highlighter->setTheme(m_repository.defaultTheme(KSyntaxHighlighting::Repository::LightTheme));
           m_highlighter->setDefinition(def);

	   if (darkModeCode) m_highlighter->setTheme(m_repository.defaultTheme(KSyntaxHighlighting::Repository::DarkTheme));

           ui->docView->setPlainText("");
           ui->docView->setFontPointSize(11);
           ui->docView->setFontFamily("Roboto Mono");

           if (!ui->ribbon->isHidden()) HideTabs();
	   //HideRibbon();

           ui->docView->setAcceptRichText(false);
           ui->docView->setPlainText(io);

	   ui->docView->setPlainText(io);
	   ui->docView->selectAll();

	   ui->sidebarframe->hide();
           ui->slimSidebar->hide();

	   plaintextmode = true;
	   QTextCursor c = ui->docView->textCursor();
           c.clearSelection();
	   c.setPosition(0);
           ui->docView->setTextCursor(c);
	   ui->doSpellCheck->hide();
	}
    }

    if (!readonly or readOnly) {
        document.filename = name;
        document.suffix   = suffix;

	//ui->startsave->hide();
        ShowSwitch();

	ui->save->hide();
        if (!readOnly) {
            //ui->autosavealert->show();
            ui->autosavelabel->setText(tr("Spectrum is AutoSaving this document."));
            ui->notification->show();
	}
        ui->openfilelocation->show();
        AddRecentFile();
    } else {
        //ui->startsave->show();
	HideSwitch();
	
        ui->save->show();
        nostart = true;
    }

    if (readOnly and !plaintextmode) ui->readingMode->click(), ui->navButton->click();

    ui->docView->document()->setModified(false);
    //setWindowTitle();
    slotSetTitle();
    updateNavigation();

    return true;
}

void O20Gui::ShowSwitch() {
    ui->label_11->show();
    ui->switch_2->show();
    UpdateAutosave();
}

void O20Gui::HideSwitch() {
    ui->label_11->hide();
    ui->switch_2->hide();
    ui->saveButton->show();
}

#include <QQuickView>

void O20Gui::ProvideContextMenu(const QPoint& pos)
{
    QPoint item = ui->recentFiles->mapToGlobal(pos);
    QMenu submenu;
    connect(submenu.addAction(QIcon(":/AxIcons/button-upload_small@1x.png"), tr("Open")), &QAction::triggered, ui->openDocument, &QToolButton::click);
    connect(submenu.addAction(QIcon(":/AxIcons/padlock-filled_small@1x.png"), tr("Open ReadOnly")), &QAction::triggered, ui->openDocumentLocked, &QToolButton::click);
    connect(submenu.addAction(QIcon(":/AxIcons/button-remove_small@1x.png"), tr("Remove from List")), &QAction::triggered, ui->removeDocument, &QToolButton::click);
    connect(submenu.addAction(QIcon(":/AxIcons/location-blue_small@1x.png"), tr("Open File Location")), &QAction::triggered, ui->openDocumentLocation, &QToolButton::click);

    submenu.exec(item);
}

void O20Gui::CreateBlankFromTemplate(QString loc, QString whereto)
{
   if (sender() == ui->singlespace) loc = "SingleSpacedBlank.odt";
   else if (sender() == ui->genotes) loc = "GeneralNotes.odt";
   else if (sender() == ui->createsimple) loc = "BasicDesignBlankTemplate.odt";
   else if (sender() == ui->buisnessletter) loc = "FormalBusinessLetter.odt";
   else if (sender() == ui->boldreport) loc = "BoldReport.odt";
   else if (sender() == ui->studentreport) loc = "SimpleStudentReport.odt";
   else if (sender() == ui->resumecover) loc = "ResumeCoverLetterGreen.odt";
   else if (sender() == ui->resume) loc = "ResumeGreen.odt";

   // SNAP doesn't properly find the users Documents location, so instead save
   // to the SNAP home, or HOME/snap/office20/

   QString _filename = (whereto == "") ? generateNewDocPath() : whereto;
   QString _suffix = QFileInfo(_filename).suffix();

    if (!nostart) {
        //ui->docView->setUsePageMode(true);
        nostart = true;
        ShowScreen("EDIT");
/*	if (loc == "SINGLESPACE") {
	    ui->docView->setPlainText("Start writing your document here.");
	} else {
            ui->docView->setPlainText("");
	    ui->docView->selectAll();
	    auto doc = ui->docView->document();
            auto blockFormat = doc->begin().blockFormat();
            blockFormat.setTopMargin(0);
	    blockFormat.setBottomMargin(0);
	    blockFormat.setLineHeight(100, QTextBlockFormat::ProportionalHeight);
            QTextCursor{doc->begin()}.setBlockFormat(blockFormat);
	    QTextCursor c = ui->docView->textCursor();
	    QTextBlockFormat bf = c.blockFormat();
            bf.setLineHeight(0, QTextBlockFormat::SingleHeight);
	    bf.setTopMargin(9);
	    bf.setBottomMargin(0);
	    c.setBlockFormat(bf);
	    c.clearSelection();
	    ui->docView->setTextCursor(c);
	}*/

	if (loc != "") openDocument(":/Templates/files/" + loc, true);

        ui->docView->document()->setModified(false);
        setWindowTitle();
        //ui->startsave->show();
	HideSwitch();
    } else {
        O20Gui* interface = new O20Gui();
	interface->CreateBlankFromTemplate(loc);
	interface->startCore();
	interface->showApp();
	return;
    }

    document.filename = _filename;
    document.suffix   = _suffix;
    firstTimeCreated = true;
    SaveDocument();

    //ui->startsave->hide();
    ShowSwitch();
    ui->save->hide();
    //ui->autosavealert->show();
    ui->openfilelocation->show();

    /*
    QMessageBox msgBox;
    msgBox.setWindowTitle("Notification");
    msgBox.setText("Your new document has been saved to<br/><span style='font-family: Ubuntu'>" + document.filename + "</span>.");
    msgBox.setIconPixmap(QPixmap(":/icons/app/48x48/ms-word.svg"));
    msgBox.exec();*/

    setWindowTitle();
    ui->notification->show();
    ui->autosavelabel->setText(QString(tr("Your document has been AutoSaved to <span style='font-family: Ubuntu'>%1</span>.")).arg(document.filename));

    enableEditing();
    AddRecentFile();

    //wordtray->show();
    //wordtray->showMessage("Autosaving " + QFileInfo(document.filename).baseName(), "Your new documents has been saved to " + document.filename + ".", QSystemTrayIcon::Information);
}

void O20Gui::EditingCommands()
{
    if (sender() == ui->fontSizeComboBox) {
        ui->docView->setFontPointSize(sender()->property("currentText").toReal());
    } else if (sender() == ui->fontComboBox) {
        ui->docView->setFontFamily(sender()->property("currentText").toString());
    } else if (sender() == ui->eraseFormat) {
        QTextCharFormat fmt;
        QTextCursor cursor = ui->docView->textCursor();
        cursor.setCharFormat(fmt);
        ui->docView->setTextCursor(cursor);
    }
}

void O20Gui::onTextChanged()
{
    //slotSetTitle();
    updateNavigation();
}

void O20Gui::updateNavigation()
{
    //if (ui->sidebarframe->isHidden()) return;

    // Get styles from QCharFormat too!
    ui->headingList->clear();
    ui->headingList_2->clear();

    headingPosList.clear();
    commentPosList.clear();

    bool autohead = ui->autohead->rootObject()->findChild<QObject*>("Switch")->property("checked").toBool();

    bool pb_before = false;
    int i = 0;
    for (QTextBlock it = ui->docView->document()->begin(); it != ui->docView->document()->end(); it = it.next()) {
        int headingLevel = it.blockFormat().headingLevel();
        QString text = it.text();

        if (autohead) {
                bool pb_now = it.blockFormat().pageBreakPolicy() == QTextFormat::PageBreak_AlwaysBefore;
                headingLevel = text.contains(tr("Chapter"), Qt::CaseInsensitive) ? 1 : headingLevel;
                headingLevel = text.contains(tr("Introduction"), Qt::CaseInsensitive) or text.contains(tr("Conclusion"), Qt::CaseInsensitive) ? 1 : headingLevel;
                headingLevel = pb_before || (i == 0 && pb_now) ||
                        (it.text() != "" && pb_now)? 1 : headingLevel;

		if (text != "" && pb_now) pb_before = false;
		else if (text != "") pb_before = pb_now;
        }

        // TODO comments with authors and which span multiline:
        if (text[0] == "`" && text[text.size()-1] == "`") {
            QListWidgetItem* item = new QListWidgetItem(it.text().mid(1, text.size() - 2));

	    ui->headingList_2->addItem(item);
            commentPosList.append(it.position());
        }

        if (plaintextmode && (text.contains("TODO") or text.contains("XXX"))) {
            QListWidgetItem* item = new QListWidgetItem(text);
            ui->headingList_2->addItem(item);
	    commentPosList.append(it.position());
        }

        if (headingLevel != 0 && headingLevel != 1) text = QString().fill(' ', headingLevel*2) + text;
        if (headingLevel != 0) {
                QListWidgetItem* item = new QListWidgetItem(text);
                ui->headingList->addItem(item);
                headingPosList.append(it.position());
        }

        i++;
    }

    if (ui->headingList->count() == 0) {
        QListWidgetItem* item = new QListWidgetItem(tr("This document has no headings."));
        ui->headingList->addItem(item);
    }
}

void O20Gui::onCursorUpdate()
{
    /*if (ui->docView->currentCharFormat().underlineStyle() != QTextCharFormat::NoUnderline) underlineColorAction->setEnabled(false);
    else underlineColorAction->setEnabled(true);*/

    for (QString stylename : stylenames) {
        if (styleCharFormats[stylename].font() == ui->docView->currentCharFormat().font()) {
	    styleButtonList[stylenames.indexOf(stylename)]->setChecked(true);
	    break;
	}
    }

    if (plaintextmode && !readOnly) {
        QTextEdit::ExtraSelection selection;
        selection.format.setBackground(QColor(m_highlighter->theme().editorColor(KSyntaxHighlighting::Theme::CurrentLine)));
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = ui->docView->textCursor();
        selection.cursor.clearSelection();

        QList<QTextEdit::ExtraSelection> extraSelections;
        extraSelections.append(selection);
        ui->docView->setExtraSelections(extraSelections);
    }

	restrict = true;
        QString mFontSize = QString::number(ui->docView->fontPointSize());
        if (mFontSize == "0") {
                ui->fontSizeComboBox->setEditText(QString::number(10));
		ui->docView->setFontPointSize(10);
	} else {
                ui->fontSizeComboBox->setEditText(mFontSize);
        }

        QString mFontFamily = ui->docView->currentCharFormat().fontFamily();
        if (mFontFamily.isEmpty() or mFontFamily.isNull()) {
            ui->fontComboBox->setCurrentFont(QFont("Open Sans"));
	    ui->docView->setFontFamily("Open Sans");
	} else {
            ui->fontComboBox->setCurrentFont(QFont(mFontFamily));
	}

        ui->setBold->setChecked((ui->docView->fontWeight() == QFont::Bold) ? true : false);
        ui->setItalic->setChecked(ui->docView->fontItalic());
        ui->setUnderline->setChecked(ui->docView->currentCharFormat().underlineStyle() != QTextCharFormat::NoUnderline);
        ui->setStrikeout->setChecked(ui->docView->currentCharFormat().fontStrikeOut());
        ui->setSubscript->setChecked(ui->docView->currentCharFormat().verticalAlignment() == QTextCharFormat::AlignSubScript);
        ui->setSuperscript->setChecked(ui->docView->currentCharFormat().verticalAlignment() == QTextCharFormat::AlignSuperScript);

        if (ui->docView->textCursor().currentList() == nullptr) {
            ui->bulletedlist->setChecked(false);
        } else {
            ui->bulletedlist->setChecked(true);
        }

        Qt::Alignment align = ui->docView->alignment();
        if (align == Qt::AlignLeft) {
                ui->alignleft->setChecked(true);
	} else if (align == Qt::AlignRight) {
                ui->alignright->setChecked(true);
        } else if (align == Qt::AlignCenter || align == Qt::AlignHCenter) {
                ui->aligncenter->setChecked(true);
        } else if (align == Qt::AlignJustify) {
                ui->alignjustify->setChecked(true);
        }
	restrict = false;

}
