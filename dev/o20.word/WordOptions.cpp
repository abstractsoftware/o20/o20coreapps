#include "O20Gui.h"
#include "src/ui_O20Ui.h"

#include <QQuickItem>
#include <QQmlProperty>

#include <O20>
#include "QMLSearch.h"

void O20Gui::LoadRecentFiles()
{
    QSettings settings;
    settings.beginGroup("RecentFileIO");
    recentfilenames = settings.value("recentFiles", recentfilenames).toStringList();
    settings.endGroup();

    // Check to see if all of the files exist
    for (QString path : recentfilenames)
       if (!QFileInfo(path).exists())
            recentfilenames.removeAt(recentfilenames.indexOf(path));

    UpdateRecentFiles();
    UpdateRecentFilesWidget();
}

void O20Gui::AddRecentFile(bool force)
{
    if (hasAlreadyRecented and !force) return;

    hasAlreadyRecented = true;

    QString path = document.filename;

    // push existing docs to the front
    if (recentfilenames.contains(path)) {
        recentfilenames.removeAt(recentfilenames.indexOf(path));
	recentfilenames.prepend(path);
    } else {
        recentfilenames.prepend(path);
    }

    if (recentfilenames.length() >= 25) recentfilenames.removeLast(); // remove the oldest
    UpdateRecentFiles();
    UpdateRecentFilesWidget();

}

void O20Gui::UpdateRecentFiles() {
    QSettings settings;
    settings.beginGroup("RecentFileIO");
    settings.setValue("recentFiles", recentfilenames);
    settings.endGroup();
}

void O20Gui::UpdateRecentFilesWidget()
{
    ui->recentFiles->clear();
    for (QString path : recentfilenames) {
        QString fname = QFileInfo(path).baseName();
	if (fname.size() > 12) QString subString = fname.mid(0,10);

	QString ntabs = "\t\t\t";
	if (fname.size() < 4) ntabs = "\t";

        QListWidgetItem* item = new QListWidgetItem("\t\t\t\t\t");
	QString sfx = QFileInfo(path).suffix();
	QString icnPath = ":/axialis/Basic/large/document.png";
	if (sfx == "odt" or sfx == "ott" or sfx == "docx" or sfx == "dotx") icnPath = ":/axialis/Basic/large/document-text-picture.png";
	else if (sfx == "html" or sfx == "xhtml") icnPath = ":/axialis/Website and Email/large/page-html.png";
	else if (sfx == "md") icnPath = ":/axialis/Development/large/markup.png"; // markdown NOT markup (?)
	else if (sfx == "c") icnPath = ":/axialis/Development/large/file-type-c.png";
	else if (sfx == "cpp" or sfx == "cxx" or sfx == "c++" or sfx == "hpp") icnPath = ":/axialis/Development/large/file-type-cpp.png";
	else if (sfx == "h") icnPath = ":/axialis/Development/large/file-type-h.png";
	else if (sfx == "py") icnPath = ":/axialis/Development/large/script-filled.png";
	else icnPath = ":/axialis/Website and Email/large/page-code.png";

	QString dir = QFileInfo(path).absoluteDir().canonicalPath();

        QLabel* label = new QLabel(QString(tr("%1<br/><span style='font-size: 8pt; color=lightgray; white-space: pre;'>%2    %3</span>")).arg(fname).arg(QFileInfo(path).lastModified().toString(tr("ddd MMMM d yyyy 'at' hh:mm"))).arg(dir));
	label->setFocusPolicy(Qt::NoFocus);

	QIcon icon = QIcon(icnPath);
	icon.addPixmap(icnPath,QIcon::Selected);
	item->setIcon(icon);

        ui->recentFiles->addItem(item);
	ui->recentFiles->setItemWidget(item, label);

        //ui->recentFiles->setItemWidget(item, label);
    }

    if (ui->recentFiles->count() == 0) ui->recentFiles->hide(), ui->norecents->show();
    else ui->norecents->hide();

    ui->recentFiles->setMinimumHeight(ui->recentFiles->count()*57);
}

void O20Gui::WriteOptions()
{
    //bool displayAnim = ui->settingsview->rootObject()->findChild<QObject*>("displayAnim")->property("checked").toBool();
    bool talkToSpectrum = ui->assistiveMode->isChecked();
    bool talkToSpectrum_2 = ui->checkBox->isChecked();
    darkModeCode = ui->darkModeCode->isChecked();


    QString userNickname = ui->userNickname->text();
    QString userName = ui->userFullName->text(); //ui->settingsview->rootObject()->findChild<QObject*>("userName")->property("text").toString();
    QString userInitials = ui->userInitials->text(); //ui->settingsview->rootObject()->findChild<QObject*>("userInitials")->property("text").toString();
    bool recoverWindowState = ui->recoverWindowState->isChecked(); //ui->settingsview->rootObject()->findChild<QObject*>("recoverWindowState")->property("checked").toBool();
    bool showSS = ui->showStartScreen->isChecked(); //ui->settingsview->rootObject()->findChild<QObject*>("showStartScreen")->property("checked").toBool();
    bool displaySS = ui->showSplashScreen->isChecked(); //ui->settingsview->rootObject()->findChild<QObject*>("displaySplashScreen")->property("checked").toBool();
    bool noPhoto = ui->noUserPhoto->isChecked(); //ui->settingsview->rootObject()->findChild<QObject*>("noPhoto")->property("checked").toBool();

    bool _autosaveAlways = ui->alwaysAutosave->isChecked();////ui->settingsview->rootObject()->findChild<QObject*>("alwaysAutosave")->property("checked").toBool();
    bool _saveLoc = ui->askSaveLocation->isChecked(); //ui->settingsview->rootObject()->findChild<QObject*>("saveLoc")->property("checked").toBool();

    QString userobjectname = "photo1";
    for(int i = 1; i < 18; ++i) {
         QString localname = "photo"+QString::number(i);
	 if (ui->photowid->findChild<QObject*>(localname)->property("checked").toBool()) {
             userobjectname = localname;
	     break;
	 }
    }

    QSettings settings;
    settings.beginGroup("UserSettings");
    settings.setValue("TryExtractFromDocx", ui->tryExtractDocx->isChecked());
    settings.setValue("UserNickname", userNickname);
    settings.setValue("DetectLang", ui->detectLang->isChecked());
    settings.setValue("UserName", userName);
    settings.setValue("UserInitials", userInitials);
    settings.setValue("DarkModeCode", darkModeCode);
    settings.setValue("ShowStartScreen", recoverWindowState);
    //settings.setValue("UseAnimations", displayAnim);
    settings.setValue("RecoverState", recoverWindowState);
    settings.setValue("SplashScreen", displaySS);
    settings.setValue("UserPhotoIcon", userobjectname);
    settings.setValue("NoPhoto", noPhoto);


    settings.setValue("AlwaysAutosave", _autosaveAlways);
    settings.setValue("AskSaveLocation", _saveLoc);

    //settings.setValue("Autosave", ui->enableautosave->isChecked());
    settings.setValue("ShowSpectrumSidebar", ui->sidebarframe->isHidden());

    if (settings.value("TalkToSpectrum", false).toBool()) {
        settings.setValue("TalkToSpectrum", !talkToSpectrum_2);
        //if (!ui->name->isHidden()) settings.setValue("UserName", ui->name->text());
    } else
        settings.setValue("TalkToSpectrum", talkToSpectrum);

    //settings.setValue("SidebarIndex", ui->sidebartabs->currentIndex());
    // XXX fix bug where if the user exits from ReadingMode, the tabs won't be saved
    if (!plaintextmode and !isReadingMode) {
        settings.setValue("ShowRibbon", ui->ribbon->isHidden());
        settings.setValue("ShowTabs", tabshidden);
    }

    settings.setValue("SpectrumHistory", searchlist);
    //settings.setValue("BlueStartScreen", ui->showbluescreen->isChecked());
   // settings.setValue("FirstTime", false);
    settings.endGroup();

    settings.beginGroup("WindowSettings");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

void O20Gui::ReadOptions()
{
    QSettings settings;
    settings.beginGroup("UserSettings");
    QString userNickname = settings.value("UserNickname", "").toString();
    QString userName = settings.value("UserName", "").toString();
    QString userInitials = settings.value("UserInitials", "").toString();
    bool showStartScreen = settings.value("ShowStartScreen", true).toBool();
    darkModeCode = settings.value("DarkModeCode", false).toBool();
    //bool displayAnim = settings.value("UseAnimations", true).toBool();
    bool recoverState = settings.value("RecoverState", true).toBool();
    bool splashScreen = settings.value("SplashScreen", true).toBool();
    noPhoto = settings.value("NoPhoto", true).toBool();
    //ui->enableautosave->setChecked(settings.value("Autosave", true).toBool());
    bool a = settings.value("ShowSpectrumSidebar", false).toBool();
    if (a) ui->sidebarframe->show(), ui->sidebarframe->move(QPoint(ui->sidebarframe->pos().x()-ui->sidebarframe->width(), ui->sidebarframe->pos().y()+10)), ui->sidebarframe->hide();
    //ui->sidebartabs->setCurrentIndex(settings.value("SidebarIndex", 1).toInt());
    bool hideribbon = settings.value("ShowRibbon", false).toBool();
    tabshidden = settings.value("ShowTabs", false).toBool();


    ui->detectLang->setChecked(settings.value("DetectLang", true).toBool());
    ui->tryExtractDocx->setChecked(settings.value("TryExtractFromDocx", false).toBool());
    bool alwaysautosave  = settings.value("AlwaysAutosave", true).toBool();
    bool asksavelocation = settings.value("AskSaveLocation", false).toBool();

    //ui->switch_2->rootObject()->findChild<QObject*>("Switch")->setProperty("checked", alwaysautosave);
    ui->alwaysAutosave->setChecked(alwaysautosave);
    alwaysAutosave = alwaysautosave;
    askSaveLocation = asksavelocation;

    ui->askSaveLocation->setChecked(askSaveLocation);
   // ui->settingsview->rootObject()->findChild<QObject*>("saveLoc")->setProperty("checked", asksavelocation);


    ui->darkModeCode->setChecked(darkModeCode);
    QString userobjectname = settings.value("UserPhotoIcon", "photo1").toString();
    bool firsttime = settings.value("FirstTime", true).toBool();
    searchlist = settings.value("SpectrumHistory", searchlist).value<QStringList>();
    //ui->showbluescreen->setChecked(settings.value("BlueStartScreen", false).toBool());

    bool talkToSpectrum = false; // TODO settings.value("TalkToSpectrum", false).toBool();
    settings.endGroup();

    spectrumOnly = talkToSpectrum;

    //if (O20::findChild<QObject*>(ui->settingsview->rootObject(), userobjectname))
    //O20::findChild<QObject*>(ui->photowid, userobjectname)->setProperty("checked", true);
    ui->photowid->findChild<QObject*>(userobjectname)->property("checked").toBool();
    QStringList list = QStringList() <<
        ":/axialis/User Management/small/people-man-1.png" <<
        ":/axialis/User Management/small/people-man-2.png" <<
        ":/axialis/User Management/small/people-man-3-filled.png" <<
        ":/axialis/User Management/small/people-man-4-filled.png" <<
        ":/axialis/User Management/small/people-man-5-filled.png" <<
        ":/axialis/User Management/small/people-man-6-filled.png" <<
        ":/axialis/User Management/small/people-man-7-filled.png" <<
        ":/axialis/User Management/small/people-man-8-filled.png" <<
        ":/axialis/User Management/small/people-man-9-filled.png" <<
        ":/axialis/User Management/small/people-woman-1-filled.png" <<
        ":/axialis/User Management/small/people-woman-2-filled.png" <<
        ":/axialis/User Management/small/people-woman-3-filled.png" <<
        ":/axialis/User Management/small/people-woman-4-filled.png" <<
        ":/axialis/User Management/small/people-woman-5-filled.png" <<
        ":/axialis/User Management/small/people-woman-6-filled.png" <<
        ":/axialis/User Management/small/people-woman-7-filled.png" <<
        ":/axialis/User Management/small/people-woman-8-filled.png" <<
        ":/axialis/User Management/small/people-woman-9-filled.png";

    usericon = userobjectname;
    usericon.remove("photo");
    usericon = list.at(usericon.toInt());
    //qDebug() <<  O20::findChild<QObject*>(ui->settingsview->rootObject(), userobjectname)->property("icon"); //->property("name").toString(); 
    /*for (QObject* object : ui->settingsview->rootObject()->findChildren<QObject*>(QRegExp(".*"))) {
        qDebug() << object->objectName();
    }*/
    ui->noUserPhoto->setChecked(noPhoto);
    ui->photowid->setHidden(!ui->noUserPhoto->isChecked());

    ui->assistiveMode->setChecked(talkToSpectrum);
    ui->showStartScreen->setChecked(showStartScreen);
    ui->showSplashScreen->setChecked(splashScreen); // TODO actually implement this!!
    ui->recoverWindowState->setChecked(recoverState);
    ui->userNickname->setText(userNickname);
    ui->userFullName->setText(userName);
    ui->userInitials->setText(userInitials);

    //ui->settingsview->rootObject()->findChild<QObject*>("noPhoto")->setProperty("checked", noPhoto);
    //ui->settingsview->rootObject()->findChild<QObject*>("displayAnim")->setProperty("checked", displayAnim);
    //ui->settingsview->rootObject()->findChild<QObject*>("talkToSpectrum")->setProperty("checked", talkToSpectrum);
    ui->checkBox->setChecked(!talkToSpectrum);
    //ui->settingsview->rootObject()->findChild<QObject*>("showStartScreen")->setProperty("checked", showStartScreen);
    //ui->settingsview->rootObject()->findChild<QObject*>("recoverWindowState")->setProperty("checked", recoverState);
    //ui->settingsview->rootObject()->findChild<QObject*>("displaySplashScreen")->setProperty("checked", splashScreen);

    ui->username->setText(userName);
    //ui->settingsview->rootObject()->findChild<QObject*>("userName")->setProperty("text", userName);
    //ui->settingsview->rootObject()->findChild<QObject*>("userInitials")->setProperty("text", userInitials);


    ui->helpindex->setCurrentIndex(0);

    if (talkToSpectrum) defaultHome = 3;

/*    if (!ui->showbluescreen->isChecked()) {
        //ui->home->setStyleSheet(ui->home->styleSheet()+"#home{background-color: transparent;}QWidget{color: black}QToolButton:hover{background-color:#d5e1f2;}QToolButton:pressed {background-color: #c0d3f0;}");
        //ui->blankfromtemplate->setIcon(QIcon(":/basic/document-text-picture.png"));
        //ui->openstart->setIcon(QIcon(":/basic/computer-browse.png"));
        //ui->startsave->setIcon(QIcon(":/basic/save-filled.png"));
    }*/

    if (!hideribbon) {
        if (tabshidden) HideTabs();
        else ShowTabs();
    } else HideRibbon();

    //ui->optionsUserName->setText("Miles Avery");
    //ui->greeting->setText("Good Morning.");
    //ui->user->setText(ui->optionsUserName->text());
    //ui->screenswitch->setAnim(displayAnim);

    if (recoverState) {
        settings.beginGroup("WindowSettings");
        resize(settings.value("size", QSize(1,1)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        settings.endGroup();
    }

    ui->aboutO20->setText(ui->aboutO20->text().arg(O20_VERSION_STR, qEnvironmentVariableIsSet("SNAP") ? "SnapCraft" : "Flatpak", ui->userNickname->text()));
}
