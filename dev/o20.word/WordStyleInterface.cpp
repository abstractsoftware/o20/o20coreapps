/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "O20Gui.h"
#include "src/ui_O20Ui.h"
#include <converter.h>

#include <QtWidgets>

bool O20Gui::checkReturnKey(QKeyEvent *keyEvent) {
    if (keyEvent->key() == Qt::Key_Return)
        if (ui->docView->textCursor().blockFormat().headingLevel() != 0) {
            QTextCursor cc = ui->docView->textCursor();
            cc.insertBlock(styleBlockFormats["Normal"]);
	    cc.setCharFormat(styleCharFormats["Normal"]);
            ui->docView->setTextCursor(cc);
	    return true;
	}
    return false;
}

void O20Gui::GenerateAutoStyles(OOO::StyleInformation* mStyleInformation)
{
   if (stylenames.size() == 0) {
        stylenames << "Normal" << "Heading 1" << "Heading 2" << "Heading 3" << "Heading 4" << "Heading 5" <<
                  /*"Heading 6" << "Heading 7" << "Heading 8" << "Heading 9" <<*/ "Title" << "Subtitle" <<
                  "Emphasis" <<  "Strong" << "Quote" << "Reference" << "Book Title";
    }

    QStringList styles_tr = QStringList() << tr("Normal") << tr("Heading 1") << tr("Heading 2") << tr("Heading 3") << tr("Heading 4") << tr("Heading 5") <<
                tr("Title") << tr("Subtitle") << tr("Emphasis") <<  tr("Strong") << tr("Quote") << tr("Reference") << tr("Book Title");


    QList<int> headings;
    headings << 0 << 1 << 2 << 3 << 4 << 5 << 1 << 0 << 0 << 0 << 0 << 0 << 0 << 0;

    /*
     Normal: 11pt
     No Spacing: line spacing: 100%
     Heading 1: color: #2f5496, 16pt, Light
     Heading 2: #5367a1, 13pt, Light
     Heading 3: #647492, 12pt, Light
     Heading 4: #758eb7, Italic, Light, 11pt
     Heading 5: #5976a9, Light, 11pt
     Heading 6: #8c98ae, Light, 11pt
     Heading 7: #808da5, Italic, 11pt
     Heading 8: 10.5pt, Light
     Heading 9: 10.5pt, Italic, Light
     Title: 28pt, Light
     Subtitle: #7c7d7d, 11pt (Italic?)
    Subtle Emphasis: Italic
    Emphasis: Italic
    Intense Emphasis: #547cc5, Italic
    Strong: Bold

    Quote: Italic, Center, Before=10pt After=8pt, Spacing=108%
    Intense Quote: (Same as Quote) + #567cc5

    SUBTLE REFERENCE: All Caps, #5a5a5a
    INTENSE REFERENCE: All Caps, #4472c4
    Book Title: Bold, Italic
    List Paragraph: (list with no bullets)
    */

    QMenu* stylemenus = new QMenu(this);

    QWidget* widget = new QWidget(this);
    QGridLayout* layout = new QGridLayout(widget);
    int row = 0, column = 0, i = 0;

    styleButtonList.clear();
    for (QString stylename : stylenames) {
        QTextCharFormat fmt;
        QTextBlockFormat bfmt = ui->docView->textCursor().blockFormat();

        /*TODO bfmt.setHeadingLevel(...); then have a sidebar with heading navigation */

        if (stylename == "Normal") ; // do nothing
        else if (stylename == "Heading 1") {
            fmt.setForeground(QBrush("#2f5496"));
            fmt.setFontPointSize(16);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(1);
        } else if (stylename == "Heading 2") {
            fmt.setForeground(QBrush("#5367a1"));
            fmt.setFontPointSize(13);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(2);
        } else if (stylename == "Heading 3") {
            fmt.setForeground(QBrush("#647492"));
            fmt.setFontPointSize(12);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(3);
        } else if (stylename == "Heading 4") {
            fmt.setForeground(QBrush("#758eb7"));
            fmt.setFontPointSize(11);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(4);
        } else if (stylename == "Heading 5") {
            fmt.setForeground(QBrush("#5976a9"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(5);
        } else if (stylename == "Heading 6") {
            fmt.setForeground(QBrush("#8c98ae"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(6);
        } else if (stylename == "Heading 7") {
            fmt.setForeground(QBrush("#808da5"));
            fmt.setFontPointSize(11);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(7);
        } else if (stylename == "Heading 8") {
            fmt.setForeground(QBrush("#2b579a"));
            fmt.setFontPointSize(10.5);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(8);
        } else if (stylename == "Heading 9") {
            fmt.setForeground(QBrush("#2b579a"));
            fmt.setFontPointSize(10.5);
            fmt.setFontItalic(true);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(9);
        } else if (stylename == "Title") {
            fmt.setFontPointSize(28);
            fmt.setFontFamily("Open Sans Light");
            bfmt.setHeadingLevel(1);
        } else if (stylename == "Subtitle") {
            fmt.setForeground(QBrush("#7c7d7d"));
            fmt.setFontPointSize(11);
            fmt.setFontFamily("Open Sans Light");
            //bfmt.setHeadingLevel(1);
        } else if (stylename == "Emphasis") {
            fmt.setFontItalic(true);
        } else if (stylename == "Strong") {
            fmt.setFontWeight(QFont::Bold);
        } else if (stylename == "Quote") {
            fmt.setFontItalic(true);
            bfmt.setAlignment(Qt::AlignHCenter);
            bfmt.setTopMargin(10);
            bfmt.setBottomMargin(8);
            bfmt.setLineHeight(108, QTextBlockFormat::ProportionalHeight);
        } else if (stylename == "Reference") {
            fmt.setFontCapitalization(QFont::AllUppercase);
            fmt.setForeground(QBrush("#5a5a5a"));
        } else if (stylename == "Book Title") {
            fmt.setFontItalic(true);
            fmt.setFontWeight(QFont::Bold);
	}

        if (mStyleInformation != nullptr) {
             QString s2 = stylename;
             QString ooo = s2.remove(" ");
             if (mStyleInformation->containsStyleFormatProperty(ooo)) {
                    auto textFormat = mStyleInformation->styleProperty(ooo).textFormat();
		    //qDebug() << textFormat.mFontFamily;
		    textFormat.apply(&fmt);
		    auto paraFormat = mStyleInformation->styleProperty(ooo).paraFormat();
                    paraFormat.apply(&bfmt);
                    //if (textFormat.mHasFontFamily)
		    //qDebug() << textFormat.mFontFamily;
		    //qDebug() << textFormat.mFontFamily;
	    }
	}

        styleCharFormats[stylename] = fmt;
        styleBlockFormats[stylename] = bfmt;

        //QWidgetAction* styleaction = new QWidgetAction(this);
	styleButtonList.append(new QToolButton(this));
	styleButtonList[i]->setCheckable(true);
	styleButtonList[i]->setAutoExclusive(true);
	styleButtonList[i]->setStyleSheet("QToolButton, QPushButton {\n  border-radius: 5px;\n margin: 1px;\nbackground-color: none;\n  width: 100px;\n  height: 40px;\n  padding: 4px;\n  border: none;\n  background-color: none;\n}\n\n\nQToolButton:hover {\n  background-color: #ecebe8;\n}\n\nQToolButton:checked, QPushButton:checked {\n  background-color: #e1dfdd;\n}\nQToolButton::menu-arrow, QToolButton::menu-indicator\n{\n    image: none;\n    width: 0px;\n    height: 0px;\n    background: transparent;\n}\n\nQToolButton[popupMode='2']\n{\n    padding-right: 0px;\n}");
	styleButtonList[i]->setText(styles_tr.at(i));
	styleButtonList[i]->setFont(fmt.font());
        //styleaction->setDefaultWidget(styleButtonList.last());
        //stylemenus->addAction(styleaction);
	layout->addWidget(styleButtonList[i], column, row);
        //styleButtonList.last()->setPopupMode(QToolButton::InstantPopup);

        styleButtonList[i]->setContextMenuPolicy(Qt::CustomContextMenu);
        QMenu* contextmenu = new QMenu(this);
        connect(contextmenu->addAction(tr("Update style")), &QAction::triggered, [=]() { UpdateStyles(stylename); });
        connect(contextmenu->addAction(tr("Update style to match selection")), &QAction::triggered, [=]() { UpdateStyles(stylename, true); });
	//styleButtonList[i]->setMenu(contextmenu);

	connect(styleButtonList[i], &QToolButton::customContextMenuRequested, [=]() {
            contextmenu->popup(QCursor::pos());
	});

	connect(styleButtonList[i], &QToolButton::released, [=]() { ConnectStyleSlots(stylename); });

	row++;
	i++;
	if (row == 3) row = 0, column++;
	
	//QAction* styleaction = stylemenus->addAction(stylename);
	//styleaction->setFont(fmt.font());
    }

    // ONLY SET MENU IF NULL
    QWidgetAction* styleaction = new QWidgetAction(this);
    styleaction->setDefaultWidget(widget);
    stylemenus->addAction(styleaction);
    connect(stylemenus->addAction(tr("Restore default styles")), SIGNAL(triggered()), SLOT(GenerateAutoStyles()));

    QTextCursor c = ui->docView->textCursor();
    for (QTextBlock it = ui->docView->document()->begin(); it != ui->docView->document()->end(); it = it.next()) {
        c.setPosition(it.position());
        QFont f = c.charFormat().font();

	for (int i = 1; i < stylenames.size(); ++i) {
	    QString stylename = stylenames[i];
            if (f == styleCharFormats[stylename].font()) {
                auto a = c.blockFormat();
                a.setHeadingLevel(headings[stylenames.indexOf(stylename)]);
                c.setBlockFormat(a);
		break;
	    }
	}
    }

    ui->stylebutton->setMenu(stylemenus);
}

void O20Gui::UpdateStyles(QString stylename, bool autodetect) {
    /*QString currentstylename = "Normal";
    for (QString stylename : stylenames) {
        if (styleCharFormats[stylename] == ui->docView->currentCharFormat()) currentstylename = stylename;
    }*/

    QString currentstylename = stylename;

    if (true) {
        QTextCharFormat currentFormat = styleCharFormats[currentstylename];
        if (!autodetect) {
            bool ok;
            QFont font = QFontDialog::getFont(&ok, styleCharFormats[currentstylename].font(), this);
	    if (ok) styleCharFormats[currentstylename].setFont(font);
	    else return;
	} else {
	    styleCharFormats[currentstylename] = ui->docView->currentCharFormat();
	    styleBlockFormats[currentstylename] = ui->docView->textCursor().blockFormat();
	    if (stylename.contains("Heading")) styleBlockFormats[stylename].setHeadingLevel(stylename.at(stylename.size()-1).digitValue()); // get the heading value
	    else if (stylename.contains("Title")) styleBlockFormats[stylename].setHeadingLevel(1);
	}

        QTextCursor c = ui->docView->textCursor();
        int pos = c.position();
        c.clearSelection();
        for (QTextBlock it = ui->docView->document()->begin(); it != ui->docView->document()->end(); it = it.next()) {
            c.setPosition(it.position());
            if (c.charFormat().font() == currentFormat.font()) {
                c.setPosition(it.position());
                c.select(QTextCursor::BlockUnderCursor);
                c.setCharFormat(styleCharFormats[currentstylename]);
                c.clearSelection();
                c.setBlockFormat(styleBlockFormats[currentstylename]);
            }
        }

        c.clearSelection();
        c.setPosition(pos);
    }

    styleButtonList[stylenames.indexOf(stylename)]->setFont(styleCharFormats[stylename].font());
}

void O20Gui::ConnectStyleSlots(QString stylename)
{
   //QString stylename = sender()->property("text").toString();

   /* Easy to apply styles
      XXX something better than merge?
    */

   //qDebug() << stylename << "\n" << styleCharFormats[stylename];

   QTextCursor cc = ui->docView->textCursor();
   bool hadSelection = cc.hasSelection();

   cc.clearSelection();
   cc.select(QTextCursor::BlockUnderCursor);
   cc.setCharFormat(styleCharFormats[stylename]);
   cc.clearSelection();
   cc.setBlockFormat(styleBlockFormats[stylename]);
   ui->docView->setTextCursor(cc);

   onCursorUpdate(); // update buttons
}
