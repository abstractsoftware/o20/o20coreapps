#include "O20Gui.h"
#include "src/ui_O20Ui.h"

#include <QMessageBox>

bool O20Gui::eventFilter(QObject *object, QEvent *event) {
    if (object == ui->docView && event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return)
            if (ui->docView->textCursor().blockFormat().headingLevel() != 0) {
                QTextCursor cc = ui->docView->textCursor();
                cc.insertBlock(styleBlockFormats["Normal"]);
                cc.setCharFormat(styleCharFormats["Normal"]);
                ui->docView->setTextCursor(cc);
		return true;
	    }
    }

    return QMainWindow::eventFilter(object, event);
}

void O20Gui::closeEvent(QCloseEvent* event)
{
    bool mod = ui->docView->document()->isModified();
    if (wasDeleted) {
        // skip
    } else if ((!autosave && mod && document.filename != "") or (document.filename == "" && nostart)) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Unsaved document"));
        msgBox.setText(tr("Sure you want to exit?"));
        if (document.filename == "") msgBox.setInformativeText(tr("This document hasn't been saved yet, so I couldn't AutoSave it."));
	else msgBox.setInformativeText(tr("I haven't been AutoSaving this document. If you exit now, all changes will be lost."));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setIconPixmap(QPixmap(":/AxIcons/save-all-filled_large@1x.png"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Save:
            if (!SaveDocument()) event->ignore();
            break;
        case QMessageBox::Cancel:
            event->ignore();
            break;
        }
    } else if (nostart && document.filename != "" && autosave) {
        SaveDocument(); // save on exit
    }

    //if (nostart && document.filename != "" && autosave) SaveDocument();

    UpdateRecentFiles();
    WriteOptions();
}

/*Display window title on interface */
void O20Gui::setWindowTitle(QString name)
{
    if (!nostart && name == "" && document.filename == "") {
        name = "O20.Word";
	goto NoMore;
    }

    if (name == "") {
        if (document.filename == "") name = defaultname;
	else name = "'"+QFileInfo(document.filename).fileName()+"'";

        if (readOnly) {
            name += tr("  (ReadOnly)");
            goto NoMore;
        }

        if (plaintextmode) name += tr(" (Code)");

        if (document.filename != "" && autosave) {
            name += tr("  (AutoSave)");
	    goto NoMore;
	} else {
           if (!ui->docView->document()->isModified()) {
               if (document.filename == "")
                   name += tr(" - Word");
               else
                   name += tr(" - Saved to your Linux PC");
	    } else {
                name += tr(" - Modified");
	    }
        } 
    }

    NoMore:

    QMainWindow::setWindowTitle(name);
}
