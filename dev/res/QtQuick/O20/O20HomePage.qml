import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs  1.2
import QtQuick.Layouts  1.12

import "Buttons" as O20BC
//import "Dialogs" as O20DC
import "Other"   as O20OC

import "o20Scripting.js" as JS

Rectangle {
    property var buttons; /* these are the sidebar buttons */

    Pane {
        id: screens

        anchors.fill: parent                

        ColumnLayout {
            visible: buttons[0].checked
            anchors.fill: parent

            Pane {
                height: 92
                Layout.fillWidth: true
                //color: "white"

                Label {
                    id: spectrumEnterGreeting

                    font.pointSize: 14
                    font.family: "Open Sans"

                    text: qsTr("Welcome!")

                    verticalAlignment: Text.AlignVCenter
                    topPadding: 50
                    leftPadding: 40
                }
            }

            Pane {

                leftPadding: 40
                background: Rectangle { color: "transparent" }

                ColumnLayout {
                    spacing: 25

                    RowLayout {
                        spacing: 10
                            
                        O20BC.Button {
                            icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/list-add.svg"
                            tip: qsTr("New")
                        }

                        O20BC.Button {
                            objectName: "upload"
                            icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/quickopen-file.svg"
                            tip: qsTr("Upload")
                        }
                            
                        Item { Layout.fillWidth: true }     
                    }
                }
            }
                    
            Item { Layout.fillHeight: true }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 92-50

            visible: buttons[1].checked

            O20OC.PageHeading { txt: qsTr("New") }
            O20OC.Template { txt: qsTr("Blank Document") }

            Flickable {
                Layout.fillWidth: true
                Layout.fillHeight: true

                contentWidth: templatesGrid.width; contentHeight: templatesGrid.height

                clip: true
                ScrollBar.vertical: ScrollBar { }

                GridLayout {
                    id: templatesGrid
                    width: parent.width

                    columns: Math.floor(screens.width/(152+100+32))

                    columnSpacing: 20

                    O20OC.Template { txt: qsTr("Single Spaced");       icn: "../../../Templates/pic/LT02786999.png" }
                    O20OC.Template { txt: qsTr("General Notes");       icn: "../../../Templates/pic/mw00002138.png" }
                    O20OC.Template { txt: qsTr("Basic Blank");         icn: "../../../Templates/pic/LT16392878.png" }
                    O20OC.Template { txt: qsTr("Business Letter");     icn: "../../../Templates/pic/mw00002133.png" }
                    O20OC.Template { txt: qsTr("Bold Report");         icn: "../../../Templates/pic/mw00002101.png" }
                    O20OC.Template { txt: qsTr("Student Report");      icn: "../../../Templates/pic/mw00002090.png"}
                    O20OC.Template { txt: qsTr("Resume");              icn: "../../../Templates/pic/mw00002024.png"}
                    O20OC.Template { txt: qsTr("Resume Cover Letter"); icn: "../../../Templates/pic/mw00002097.png" }
                }
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 92-50

            visible: buttons[2].checked
            
            O20OC.PageHeading { txt: qsTr("Open") }

            O20BC.PushButton {
                iconSource: "open-for-editing.svg";
                text: "Browse"
            }

            O20BC.PushButton {
                iconSource: "format-text-code";
                text: "Import Code";
            }

            O20BC.PushButton {
                iconSource: "edit-paste-in-place";
                text: "Display Location"
            }

            Item { Layout.fillHeight: true }

            O20BC.Button {
                checkable: true;

                implicitWidth: 56
                implicitHeight: 56

                icon.source: {
                    if (checked)
                        "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/document-encrypted"
                    else
                        "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/document-decrypt"
                }
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 92-50

            visible: buttons[3].checked

            O20OC.PageHeading { txt: qsTr("Save") }

            O20BC.PushButton {
                iconSource: "document-save-as.svg";
                text: "Save As"
            }

            O20BC.PushButton {
                 iconSource: "acrobat.svg";
                 text: "Export to PDF"
            }

            O20BC.PushButton {
                iconSource: "globe.svg";
                text: "Export to HTML"
            }

            O20BC.PushButton {
                iconSource: "code-context.svg";
                text: "Export to Text"
            }

            O20BC.PushButton {
                id: renameDocument

                iconSource: "document-edit.svg"
                text: qsTr("Rename")

                checkable: true
            }

            TextField {
                visible: renameDocument.checked
                placeholderText: "What would you like to rename your document to?"
            }

            O20BC.PushButton {
                iconSource: "document-print.svg"
                text: "Print"
            }

            O20BC.PushButton {
                iconSource: "trash-empty.svg"
                text: "Delete"
            }


            Item { Layout.fillHeight: true }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 92-50

            visible: buttons[4].checked

            O20OC.PageHeading { txt: qsTr("Pro20 Office") }

            Label { text: "<html><head/><body><p>Free Product<br/>You are using O20 and O20.Word %1 %2.</p><p><br/>Belongs to: %3</p></body></html>" }

            /* TODO: make this an app gallery where you can click on each icon and O20 will
                     tell you about that app */

            RowLayout {
                ToolButton { icon.width: 32; icon.height: 32; icon.source: "../../O20Core/apps/ms-office.svg"; icon.color: "transparent"; }
                ToolButton { icon.width: 32; icon.height: 32; icon.source: "../../O20Core/apps/ms-word.svg"; icon.color: "transparent"; }
                ToolButton { icon.width: 32; icon.height: 32; icon.source: "../../O20Core/apps/ms-powerpoint.svg"; icon.color: "transparent"; }
                ToolButton { icon.width: 32; icon.height: 32; icon.source: "../../O20Core/apps/ms-excel.svg"; icon.color: "transparent"; }
                ToolButton { icon.width: 32; icon.height: 32; icon.source: "../../O20Core/apps/ms-onenote.svg"; icon.color: "transparent"; }
            }

            RowLayout {
                spacing: 20
                O20BC.Button {
                    objectName: "aboutQt";
                    text: "Qt"

                    font.family: "Roboto Mono"
                    font.pointSize: 18

                    tip: qsTr("About Qt");
                }

                O20BC.Button {
                    icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.32/help-about.svg";
                    icon.color: "transparent"
                    tip: qsTr("Website");
                    onClicked: Qt.openUrlExternally("https://abstractsoftware.gitlab.io")
                }

                O20BC.Button {
                    tip: qsTr("Report Bug");
                    text: "Report\nBug"
                    onClicked: Qt.openUrlExternally("https://gitlab.com/abstractsoftware/o20/o20coreapps/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=")
                }

                O20BC.Button {
                    text: "Submit\nIdeas"
                    tip: qsTr("Submit Ideas");
                    onClicked: Qt.openUrlExternally("https://gitlab.com/abstractsoftware/o20/o20coreapps/issues/7")
                }
            }


            Item { Layout.fillHeight: true }

            Label {
                text: "<html><head/><body><p><span style=\" font-family:'Open Sans';\">Icons courtesy of the </span><a href=\"https://github.com/vinceliuice/McMojave-circle\"><span style=\" font-family:'Open Sans'; text-decoration: underline; color:#2980b9;\">McMojave Circle</span></a><span style=\" font-family:'Open Sans';\"> and </span><a href=\"https://www.axialis.com\"><span style=\" font-family:'Noto Sans'; text-decoration: underline; color:#2980b9;\">Axialis Software</span></a><span style=\" font-family:'Open Sans';\">.</span></p></body></html>";

                onLinkActivated: Qt.openUrlExternally(hoveredLink) 
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10
            anchors.topMargin: 92-50
    
            visible: buttons[5].checked
        
            O20OC.PageHeading { txt: qsTr("Options") }

            O20OC.DesktopScroll {
                Layout.fillWidth: true; Layout.fillHeight: true;

                ColumnLayout {
                    Label { text: "General Settings" }
                    CheckBox { text: "Recover the window state" }
                    CheckBox { text: "Show the splash screen" }
                    CheckBox { text: "Show the start screen" }

                    Label { text: "User Customization" }
                    TextField { placeholderText: "What would you like me to call you?"; implicitWidth: 300; }
                    TextField { placeholderText: "What is your full name?"; implicitWidth: 300;}
                    TextField { placeholderText: "What are your initials?"; implicitWidth: 300; }
                    Switch { text: "Use Dark Mode"; onClicked: darkMode = checked, console.log("changed")}

                    Label { text: "Save and AutoSave" }
                    CheckBox { text: "Always AutoSave" }
                    CheckBox { text: "Ask for save location" }
                }
            }
        }
    }
}
