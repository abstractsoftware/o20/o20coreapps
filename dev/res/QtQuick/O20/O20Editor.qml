import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

import QtQuick.Controls 1.4 as QQC1

import "Buttons" as O20BC
import "Dialogs" as O20DG
import "Other"   as O20OC

import "o20Scripting.js" as JS

Item {
    property var styles: ["Normal", "Title", "Subtitle", "Heading 1", "Heading 2", "Heading 3", "Emphasis", "Strong", "Quote", "Reference"]

    //property var editorClass: docview.editor
    Material.theme: Material.Light

    id: editor
    Rectangle {
        anchors.fill: parent
            
        color: "#f0f0f0";
            
        ColumnLayout {
            anchors.fill: parent
            spacing: 0
                
        O20Ribbon { objectName: "ribbonAnchor" }
                
        /*Flickable {
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: view

        Item { height: 10 }

            O20TextArea {
                 id: docview
                 objectName: "doc"


                 Layout.fillWidth: true; Layout.fillHeight: true;

                 //leftPadding: 20
                 focus: true
            }
        //} */


       //Item { height: 10 }
       // O20TextArea { objectName: "editor"; /*objectName: "wherePage";*/ Layout.fillWidth: true; Layout.fillHeight: true; }

        /*ScrollView {
            id: view

            Layout.fillWidth: true
            Layout.fillHeight: true

            ScrollBar.vertical.policy: ScrollBar.AlwaysOn

            TextArea {
                Layout.fillWidth: true
                Layout.fillHeight: true



                objectName: "editor"
                selectByMouse: true
                wrapMode: TextEdit.Wrap
                //placeholderText: "Get started with your new document by typing here."
            }
        }*/

        Item { objectName: "editor"; Layout.fillHeight: true; Layout.fillWidth: true; }

        Rectangle { Layout.fillWidth: true; height: 1; color: "#c6c6c6"; }
                
        RowLayout {
            spacing: 0

            height: 20
            //anchors.fill: parent
                        
            Label {
                text: "   45 Pages        32 Words    "
            }
                        
            Button {
                id: aa;
                icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/tools-check-spelling.svg"
                icon.height: 16
                icon.width: 16
                implicitWidth: 32;
            }
                        
            Item { width: 5; height: 50;}
                        
            Item { Layout.fillWidth: true }
                        
            Button {
                id: yy;
                icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/view-fullscreen.svg"
                icon.height: 16
                icon.width: 16
                implicitWidth: 32;
            }

            Item { width: 5 }
        }
    }
  }
}
