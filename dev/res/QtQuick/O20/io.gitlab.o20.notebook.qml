import QtQuick 2.12

import "o20Scripting.js" as JS

O20 {
    themeColor: "#7719aa"
    appName: "Notebook"
    useStartScreen: false
    useRibbon: false
}
