import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtQuick.Controls.Material 2.12

import "o20Scripting.js" as JS

SwipeView {
    id: view

    signal pageChanged(int page)

    objectName: "swipe"

    property var themeColor: "#2b579a"
    property var appName: "Word"

    property var useRibbon: true
    property var useStartScreen: true
    property var darkMode: false

    currentIndex: useStartScreen ? 0 : 1
    interactive: false

    /*function resizeSidebar() {
       s.height = height+1;
       console.log("resizing...");
    }*/

    property var documentEdit: false

    Material.theme: {
        darkMode ? Material.Dark : Material.Light /* choose whether to use darkMaterial mode */
    }

    Material.accent: themeColor

    Item {
        id: homeScreen

        RowLayout {
            spacing: 0
            O20Sidebar {
                id: s
                height: view.height
            }

            O20HomePage {
                buttons: s.buttonsRef

                x: s.sW
                width: homeScreen.width - s.sW
                height: homeScreen.height
            }
        }
    }

    O20Editor { id: editor }

    QtObject {
        id: qtobject
        property int index: view.currentIndex

        Behavior on index {
            ScriptAction {
               script: view.pageChanged(qtobject.index);
            }
        }
    }
}
