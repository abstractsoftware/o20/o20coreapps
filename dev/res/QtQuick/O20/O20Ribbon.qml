import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts  1.12

import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

import "Buttons" as O20BC
import "Dialogs" as O20DG
import "Other"   as O20OC

import "o20Scripting.js" as JS

ColumnLayout {
    spacing: 0

   property var themeColor: "#2b579a"
   Material.accent: themeColor

            Rectangle {
                Layout.fillWidth: true
                height: 42
                    
                color: themeColor

                RowLayout {
                    Material.theme: Material.Dark
                    Material.accent: "white"
                        
                    spacing: 0
                        
                    anchors.fill: parent
                        
                    Item { width: 20 }
                    Label { text: "AutoSave" }
                    Switch { id: autosave; Layout.fillHeight: true; checked: true; }
                    ToolButton {
                        id: save;
                        visible: !autosave.checked
                        icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/document-save.svg"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42
                    }
                        
                    Item { width: 5 }
                        
                    Item { Layout.fillWidth: true }
                    
                    Label { text: "the Abstract Developers"; padding: 5;  }
    
                    ToolButton {
                        id: tabs;
                        icon.source: "../../QtQuickComponents/o20.icons.breeze/breeze.actions.16/tab-detach.svg"
                        icon.height: 16
                        icon.width: 16
                        implicitHeight: 42

                        onClicked: menu.open()

                        Menu {
                            id: menu
                            y: tabs.height

                            MenuItem {
                                text: "Hide Ribbon"
                                checkable: true; autoExclusive: true;
                                onTriggered: ribbonItem.visible = false, ribbonTabs.visible = false
                            }

                            MenuItem {
                                text: "Tabs Only"
                                checkable: true; autoExclusive: true;
                                onTriggered: ribbonItem.visible = false, ribbonTabs.visible = true
                            }

                            MenuItem {
                                text: "Tabs and Commands"
                                checkable: true; autoExclusive: true;
                                onTriggered: ribbonItem.visible = true, ribbonTabs.visible = true
                            }

                            MenuItem {
                                text: "Show Sidebar"
                                checkable: true;
                           }
                     }
                 }
             }
         }

         TabBar {
             id: ribbonTabs
                    
             currentIndex: 1
             leftPadding: 20
             background: Rectangle { color: "transparent" }
                    
             O20BC.TabButton { text: qsTr("File"); onClicked: { currentIndex = 0; ribbonTabs.currentIndex = 1; } }
             O20BC.TabButton { text: qsTr("Home"); }
             O20BC.TabButton { text: qsTr("Insert"); }
             O20BC.TabButton { text: qsTr("Review"); }
             O20BC.TabButton { text: qsTr("Help") }
                   
         }
 
         Item {
             id: ribbonItem

             Layout.fillWidth: true
             height: 48

             z: 10000

             Rectangle {
                 id: ribbon
                 anchors.fill: parent
                 color: "#f0f0f0"


                 StackLayout {
                    id: ribbonO
                    anchors.fill: parent
                
                    currentIndex: ribbonTabs.currentIndex

                    Item { }
                    
                    Item {
                        RowLayout {
                            spacing: 5
                        
                            Item { width: 20 }
                        
                            Menu {
                                id: contextMenu
                                MenuItem { text: "Cut" }
                                MenuItem { text: "Copy" }
                                MenuItem { text: "Paste" }
                            }

                        
                            Menu {
                                id: spacingMenu
                                MenuItem { text: "Line Spacing" }
                                MenuItem { text: "1"; checked: true; checkable: true; autoExclusive: true; }
                                MenuItem { text: "1.15"; checkable: true; autoExclusive: true; }
                                MenuItem { text: "1.5"; checkable: true; autoExclusive: true; }
                                MenuItem { text: "2"; checkable: true; autoExclusive: true; }
                                MenuItem { text: "2.5"; checkable: true; autoExclusive: true; }
                                MenuItem { text: "3"; checkable: true; autoExclusive: true; }
                            }
                        
                            Menu {
                                id: colorMenu
                                MenuItem { text: "Default" }
                                MenuItem { text: "Select..." }
                            }
                
                            Menu {
                                id: colorFillMenu
                                MenuItem { text: "Transparent" }
                                MenuItem { text: "Select..." }
                            }

                            Menu {
                                id: listNumberingMenu
                                MenuItem { text: "Bullet" }
                                MenuItem { text: "White Bullet" }
                                MenuItem { text: "Square Bullet" }
                                MenuItem { text: "Star" }
                                MenuItem { text: "Check" }
                                MenuItem { text: "1. 2. 3."}
                                MenuItem { text: "A. B. C." }
                                MenuItem { text: "a. b. c." }
                                MenuItem { text: "I. II. III." }
                                MenuItem { text: "i. ii. iii." }
                                MenuItem { text: "Define custom bullet..." }
                            }
                        
                        
                            Menu {
                                id: styleMenu
                                MenuItem { text: JS.styles[0] } // Normal
                                MenuItem { text: JS.styles[1] } // Title
                                MenuItem { text: JS.styles[2] } // stitle
                                MenuItem { text: JS.styles[3] } // H1
                                MenuItem { text: JS.styles[4] } // H2
                                MenuItem { text: JS.styles[5] } // H3
                                MenuItem { text: JS.styles[6] } // Emphasis
                                MenuItem { text: JS.styles[7] } // Strong
                                MenuItem { text: JS.styles[8] } // Quote
                                MenuItem { text: JS.styles[9] } // Reference
                            }
                        
                            O20BC.RibbonMenuButton { menuClass: contextMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/clipboard.png"; }
                            O20OC.Line {}


                            ComboBox { model: Qt.fontFamilies(); editable: true; implicitWidth: 250; }
                                    
                            ComboBox {
                                editable: true
                                model: [8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 24, 26, 28, 32, 48, 72]
                            }
                        
                            O20OC.Line {}

                            O20BC.RibbonButtonCheckable { objectName:"formatBold"; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-bold.png"; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-italic.png"; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-underline.png"; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-strikethrough.png"; }

                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-superscript.png"; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/character-subscript.png"; }
                            O20BC.RibbonMenuButton { menuClass: colorMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/picture-colors-filled.png"; }
                            O20BC.RibbonButton { icon.source: "../../QtQuickComponents/o20.icons/Images/small/tool-eraser-auto.png"; }

                            O20OC.Line {}
                        
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/paragraph-align-left.png"; autoExclusive: true; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/paragraph-align-center.png"; autoExclusive: true; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/paragraph-align-right.png"; autoExclusive: true; }
                            O20BC.RibbonButtonCheckable { icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/paragraph-align-justified.png"; autoExclusive: true; }
                            O20BC.RibbonMenuButton { menuClass: listNumberingMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/list-numbers.png"; }
                            O20BC.RibbonMenuButton { menuClass: colorFillMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/color-fill-filled.png"; }
                            O20BC.RibbonMenuButton { menuClass: spacingMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/paragraph-spacing.png"; }

                            O20OC.Line {}

                            O20BC.RibbonMenuButton { menuClass: styleMenu; icon.source: "../../QtQuickComponents/o20.icons/Word Processing/small/styles.png"; }


                            Item { Layout.fillWidth: true }
                        }
                    }
                }
            }
           DropShadow {
                //  visible: ribbonO.visible
                anchors.fill: ribbon
                cached: true
                horizontalOffset: 3
                verticalOffset: 3
                radius: 4.0
                samples: 16
                color: "#c6c6c6"
                source: ribbon
            }
        }
}
