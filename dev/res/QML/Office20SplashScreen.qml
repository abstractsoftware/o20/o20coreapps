import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12


Rectangle {
    visible: true

    anchors.fill: parent

    radius: 5

    width: 500
    height: 300

    Material.accent: "white"
    Material.theme: Material.Light

    Universal.accent: "white"
    Universal.theme: Universal.Light


    /* example: appColor: "#2b579a", appIcon: "../AppIcons/blackandwhite/WinWordLogoSmall.contrast-black_scale-180.png", appname: "O20.Word" */
    property string appColor : "#e83f24"
    property string appIcon : "../AppIcons/blackandwhite/FirstRunLogoSmall.contrast-black_scale-180.png" 
    property string qversion: "5.13.2"
    property string appName: "O20.App"

    //"../AppIcons/blackandwhite/WinWordLogoSmall.contrast-black_scale-180.png"

    color: appColor

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Rectangle {
             Layout.fillWidth: true
             height: 16
             color: "transparent"
        RowLayout {
           anchors.fill: parent
           Item { Layout.fillWidth: true; }
           ToolButton {
               icon.source: "../BreezeDark/window-close.svg"
               icon.color: "#ffffff"
               height: 16
               width: 16
               padding: 2
               onClicked: {
                   Qt.quit();
               }
           } 
        }
        }

        Item { Layout.fillHeight: true }

        Rectangle {
           Layout.fillWidth: true

           height: 86
           color: "transparent"

           RowLayout {
           anchors.fill: parent

           Item { Layout.fillWidth: true; }

        Rectangle {
            height: 86
            width: 86
            //Layout.fillWidth: true
            color: "transparent"

            Image {
                anchors.centerIn: parent
                source: appIcon 
            }
        }

        Label {
            //Layout.fillWidth: true
            textFormat: Text.RichText
            text: "Pro20."+appName
            color: "white"
            font.pointSize: 24
            font.family: "Open Sans Light"
            horizontalAlignment: Text.AlignHCenter
        }

           Item { Layout.fillWidth: true; }

           /*ToolButton { 
               icon.source: "../BreezeDark/window-close.svg"; icon.color: "#ffffff"; 
               onClicked: {
                   Qt.quit()
               }
           }*/
           }
        }


        Label {
            Layout.fillWidth: true
            color: "white"
            
            text: qsTr("Built with " + qversion + ".")
            horizontalAlignment: Text.AlignHCenter
            font.family: "Open Sans"
            visible: false
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Rectangle {
            Layout.fillWidth: true
            ProgressBar {
                anchors.centerIn: parent
                //running: true
                height: 48
                width: 300
                indeterminate: true
            }
        }

        Item { height: 10 }
    }
}

