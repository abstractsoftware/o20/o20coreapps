import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12

ScrollView {
   id: foo

    Material.accent: Material.Blue
    Material.theme: Material.Light

    property var objects: profilePhotos.children
    property var objectNames: ["photo01", "photo02", "photo03", "photo04", "photo05", "photo06", "photo07", "photo08", "photo09", "photo10", "photo11", "photo12", "photo13", "photo14", "photo15", "photo16", "photo17", "photo18"]
    property var objectIcons: [
        "../AxIcons/people-man-1-filled_large@1x.png",
        "../AxIcons/people-man-2-filled_large@1x.png",
        "../AxIcons/people-man-3-filled_large@1x.png",
        "../AxIcons/people-man-4-filled_large@1x.png",
        "../AxIcons/people-man-5-filled_large@1x.png",
        "../AxIcons/people-man-6-filled_large@1x.png",
        "../AxIcons/people-man-7-filled_large@1x.png",
        "../AxIcons/people-man-8-filled_large@1x.png",
        "../AxIcons/people-man-9-filled_large@1x.png",
        "../AxIcons/people-woman-1-filled_large@1x.png",
        "../AxIcons/people-woman-2-filled_large@1x.png",
        "../AxIcons/people-woman-3-filled_large@1x.png",
        "../AxIcons/people-woman-4-filled_large@1x.png",
        "../AxIcons/people-woman-5-filled_large@1x.png",
        "../AxIcons/people-woman-6-filled_large@1x.png",
        "../AxIcons/people-woman-7-filled_large@1x.png",
        "../AxIcons/people-woman-8-filled_large@1x.png",
        "../AxIcons/people-woman-9-filled_large@1x.png",
    ]

    Universal.accent: Universal.Blue
    Universal.theme: Universal.Light

    visible: true
    clip: true

    background: Rectangle {
        id: rect
        color: "#f0f0f0"
    }

    ColumnLayout {
        anchors.margins: 10
        //anchors.fill: parent
        spacing: 10

        width: 350

        RowLayout {
            Image {
                source: "../AxIcons/people-man-1-filled_large@1x.png"
            }
            Label {
                font.family: "Open Sans Light"
                font.pointSize: 22
                text: "John Doe"
            }
        }

        Item { height: 10 }

        //Pane {
        //    Layout.fillWidth: true
        //    Layout.fillHeight: true

        //   Material.elevation: 6

            ColumnLayout {
                Label {
                  height: 35
                  text: "General Options"
                  font.pointSize: 16
                  font.family: "Open Sans Light"
                }

                CheckBox {
                  objectName: "displayAnim"
                  text: "Show Animations"
                  font.pointSize: 10
                  visible: false
                }

                CheckBox {
                  objectName: "talkToSpectrum"
                  text: "Talk to Spectrum"
                  font.pointSize: 10
                }

                CheckBox {
                  objectName: "recoverWindowState"
                  text: "Recover window state"
                  font.pointSize: 10
                }

                CheckBox {
                  objectName: "showStartScreen"
                  text: "Show the start screen at startup"
                  font.pointSize: 10
                }

                CheckBox {
                  objectName: "displaySplashScreen"
                  text: "Display splash screen"
                  font.pointSize: 10
                  visible: false
                }
        //}

        //Pane {
        //    Layout.fillWidth: true
        //    Layout.fillHeight: true
        //   
        //    Material.elevation: 6
      
            ColumnLayout {

                Label {
                  textFormat: Text.RichText
                  text: "User Information"
                  font.pointSize: 16
                  font.family: "Open Sans Light"
                }

                Label {
                  text: "Tell Spectrum and O20 about yourself!"
                  font.pointSize: 11
                  font.family: "Open Sans"
                }

                TextField {
                  objectName: "userName"
                  Layout.fillWidth: true
                  placeholderText: "My name is Spectrum. Tell me yours."
                  font.pointSize: 10
                  width: 250
                }

                TextField {
                  objectName: "userInitials"
                  Layout.fillWidth: true
                  placeholderText: "How should I abbreviate your name?"
                  font.pointSize: 10
                  width: 250
                }
            }


                Label {
                  text: "Choose your user profile photo"
                }

                Switch {
                    id: noPhoto
                    text: "No photo"
                }



                GridLayout {
                    id: profilePhotos
                    visible: noPhoto.checked
                    columns: 7
                    Layout.fillWidth: true
                    
                    Repeater {
                        model: 18
                        
                        RoundButton {
                            checkable: true
                            visible: index < 6 || showMore.checked
                            autoExclusive: true
                            icon.source: foo.objectIcons[index] //"../AxIcons/people-man-4-filled_large@1x.png"
                            icon.color: "transparent"

                            onClicked: {
                                console.log(objectName)
                            }
                        }
                    }

                    RoundButton {
                       id: showMore
                       checkable: true
                       icon.source: checked ? "../AxIcons/button-remove_small@1x.png" : "../AxIcons/button-add_small@1x.png"
                       icon.color: "transparent"
                    }
                }
            }
        //}
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        signal buttonClicked_enable()
    }
}
