import QtQuick 2.13
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12

Rectangle {
    id: dialog
    visible: true

    Universal.theme: Universal.Dark

    property string textInform: "Your new document has been saved to /home/xxx/Documents/O20/Docs/Document1.odt."

    //anchors.fill: parent

    color: "#2b579a"

    ColumnLayout {
        anchors.margins: 5
        anchors.fill: parent
        
        RowLayout {
            Layout.fillWidth: true
            Item { width: 5}
            Label {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                color: "white"
                text: "Spectrum is AutoSaving your document"
                font.family: "Open Sans" 
            }
        }

        Text {
            Layout.fillWidth: true
            Layout.fillHeight: true
            textFormat: Text.RichText
            font.family: "Open Sans"
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            wrapMode: Text.WordWrap
            text: textInform
        }

        RowLayout {
            Layout.fillWidth: true
            Item{ Layout.fillWidth: true}

            Button {
                text: "Show in Explorer"
            }

            Button {
                text: "OK"
                Text {
                color: "white"
                }
            }

            Item{ Layout.fillWidth: true}
        }

        Item{ height: 5 }
    }
}
