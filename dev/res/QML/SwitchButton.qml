import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Universal 2.12
import QtQuick.Controls.Material 2.12

Rectangle {

    height: 25
    width: 60

    color: "#2b579a"
    visible: true;

    Material.theme: Material.Dark
    Material.accent: "white"
    Universal.theme: Universal.Dark
    Universal.accent: Universal.Cyan

    Switch {
        anchors.fill: parent
        id: control
        objectName: "Switch"
        checked: true
        smooth: true
    }

    signal buttonClicked_enable()
}
