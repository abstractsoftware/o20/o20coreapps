import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12

//ApplicationWindow {
//    id: recta

//    width: 400;
//    height: 500;
/*
    height: Screen.height
    width: 500

    x: Screen.width - width

    flags: Qt.WA_TranslucentBackground | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint

    color: "transparent";//"#f0f0f0aa"

    ParallelAnimation {
          //NumberAnimation { target: recta; property: "x"; from: Screen.width; to: Screen.width - 400; duration: 1000 }
          NumberAnimation { target: recta; property: "width"; from: 0; to: 500; duration: 1000 }
    } */

ScrollView {
    id: parentScroll
    //anchors.fill: parent

    //  ScrollBar.horizontal.interactive: false
    //  ScrollBar.vertical.interactive: false

    Material.accent: Material.Indigo
    Universal.accent: Universal.Cyan

    padding: 10
    //height: parent.height
    visible: true
    clip: true   

    /*y: 0
    x: 0*/

    /* all avaliable profile photos*/ 
    property var objectIcons: [
        "../AxIcons/people-man-1-filled_large@1x.png",
        "../AxIcons/people-man-2-filled_large@1x.png",
        "../AxIcons/people-man-3-filled_large@1x.png",
        "../AxIcons/people-man-4-filled_large@1x.png",
        "../AxIcons/people-man-5-filled_large@1x.png",
        "../AxIcons/people-man-6-filled_large@1x.png",
        "../AxIcons/people-man-7-filled_large@1x.png",
        "../AxIcons/people-man-8-filled_large@1x.png",
        "../AxIcons/people-man-9-filled_large@1x.png",
        "../AxIcons/people-woman-1-filled_large@1x.png",
        "../AxIcons/people-woman-2-filled_large@1x.png",
        "../AxIcons/people-woman-3-filled_large@1x.png",
        "../AxIcons/people-woman-4-filled_large@1x.png",
        "../AxIcons/people-woman-5-filled_large@1x.png",
        "../AxIcons/people-woman-6-filled_large@1x.png",
        "../AxIcons/people-woman-7-filled_large@1x.png",
        "../AxIcons/people-woman-8-filled_large@1x.png",
        "../AxIcons/people-woman-9-filled_large@1x.png",
    ]

    property var currentSelectedPhoto: 0

    background: Rectangle {
        id: rect
        color: "#f0f0f0"
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 0

            ColumnLayout {
                //anchors.fill: parent
                Layout.fillWidth: parent;
                Layout.fillHeight: parent;
                
                property var headingFontSize: 12

                Label {
                    Layout.fillWidth: parent;
                    text: "O20 General Settings"
                    font.family: "Open Sans"
                    font.pointSize: parent.headingFontSize
                }

                Switch {
                    objectName: "talkToSpectrum"
                    text: "MobileMode"
                    visible: false
                }

                GridLayout {
                   Layout.fillWidth: parent
                   Layout.fillHeight: parent
                   columns: 1

                    CheckBox {
                        objectName: "recoverWindowState"
                        text: "Recover Window State"
                    }

                    CheckBox {
                        objectName: "showStartScreen"
                        text: "Show the Start Screen"
                    }

                    CheckBox {
                        objectName: "displaySplashScreen"
                        text: "Display Splash Screen"
                    }

                    CheckBox {
                        objectName: "displayAnim"      
                        text: "Display Animations"
                        visible: false
                    }
                }

                Label {
                    id: widget
                    Layout.fillWidth: parent;
                    text: "User Customization"
                    font.family: "Open Sans"
                    font.pointSize: parent.headingFontSize
                }

                /*Label {
                    Layout.fillWidth: parent
                    text: "I'm the O20 Assistant. Tell me more about yourself."
                    font.family: "Open Sans"
                    font.pointSize: parent.headingFontSize - 6
                }*/


                 TextField {
                     implicitWidth: 300
                     objectName: "userName"
                     Layout.fillWidth: parent;
                     placeholderText: "What would you like me to call you?"
                 }

                 TextField {
                     objectName: "userInitials"
                     Layout.fillWidth: parent;
                     placeholderText: "What are your initials?"
                 }

                 Switch {
                     id: noPhoto
                     checked: true
                     objectName: "noPhoto"
                      text: "No Photo"
                 }

                    GridLayout {
                        id: profilePhotos
                        visible: !noPhoto.checked
                        columns: 7
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Repeater {
                            model: 18

                            RoundButton {
                                objectName: "photo" + index.toString()
                                checkable: true
                                //visible: index < 6 || showMore.checked
                                autoExclusive: true
                                icon.source: parentScroll.objectIcons[index] //"../AxIcons/people-man-4-filled_large@1x.png"
                                icon.color: "transparent"

                                onClicked: {
                                    console.log(objectName); 
                                }
                            }
                        }

                       /*RoundButton {
                           id: showMore
                           flat: true
                           checkable: true
                           //text: checked ? "-" : "+"
                           icon.source: checked ? "../AxIcons/button-remove_small@1x.png" : "../AxIcons/button-add_small@1x.png"
                           icon.color: "transparent"
                       }*/
                   }


                Label {
                    Layout.fillWidth: parent;
                    text: "AutoSave and Save options"
                    font.family: "Open Sans"
                    font.pointSize: parent.headingFontSize
                }

                /*ComboBox {
                    implicitWidth: 450
                    displayText: "Default format: " + currentText
                    model: ["ODF OpenDocument Format", "XHTML Hypertext Markup Language"]
                }*/

                Switch {
                   objectName: "alwaysAutosave"
                   text: "Always AutoSave?"
                   checked: true
                }

                /*Switch {
                    objectName: "saveOnExit"
                    text: "Save Documents on Exit"
                    checked: true
                }*/

                Switch {
                   id: saveDefault
                   objectName: "saveLoc"
                   text: "Always Ask for Save Location"
                }

                TextField {
                   objectName: "saveDefaultLocation"
                   implicitWidth: 300
                   visible: false //!saveDefault.checked
                   placeholderText: "Enter your default save location"
                }
            }

            Item { Layout.fillHeight: true }
        //}

        signal buttonClicked_enable()
    }
}

//}
