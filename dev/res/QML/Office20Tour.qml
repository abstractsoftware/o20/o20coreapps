import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Controls.Universal 2.12

Rectangle {
   anchors.fill: parent

        color: "#e83f24"
        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#b7472a"; duration: 2000 } // powerpoint
            ColorAnimation { to: "#7719aa"; duration: 2000 } // onenote
            ColorAnimation { to: "#2b579a"; duration: 2000 } // word
            //ColorAnimation { to: "#217346"; duration: 2000 } // excel
            ColorAnimation { to: "#e83f24"; duration: 2000 } // office365
        }

   ColumnLayout {
      Layout.minimumWidth: 200
      anchors.margins: 20
      anchors.fill: parent

       Item {
          height: 50
          Layout.fillWidth: true

          Rectangle {
             anchors.fill: parent
             id: roundedRect
             height: 50
             radius: 5
             color: "#ffffff"
             smooth: true
             opacity: 0.25
          }

           Label {
              anchors.centerIn: parent
              text: "Welcome to O20"
              color: "white"
              font.pointSize: 20
              font.family: "Open Sans Light"
           }
       }

       //Text {
       Rectangle {
          Layout.fillHeight: true 
          Layout.fillWidth: true
          color: "transparent"

          Text {
             id: information
             textFormat: Text.RichText
             anchors.centerIn: parent
             font.family: "Open Sans Light"
             color: "white"
             text: "<html><p style='font-size: 20pt;'>Thanks for installing O20.</p>
                    <p style='font-size: 13pt;'>O20 is a modern and fast office suite for the Linux Desktop, built on Qt5, QML, KDE, and Snap.</p>
                    </html>"

             property var informationText: [
             "<p style='font-size: 20pt;'>Modern Interface</p><p style='font-size: 12pt;'>All of the O20 apps use a sleek, streamlined MiniRibbon UI.</p>", 
             "<p style='font-size: 20pt;'>Advanced Learning</p>
              <p style='font-size: 12pt;'>With SpectrumCore Assistant, O20 tries to learn how you like to get things done.</p>"]
             property int index: 0

             Timer { 
                 repeat: true
                 interval: 5000
                 running: true

                 onTriggered: {
                     if (information.index == information.informationText.length) information.index = 0;
                     information.text = information.informationText[information.index];
                     information.index += 1;
                 }
             }
          }
       }

       RowLayout {
          Layout.fillWidth: true

          Item {
            Layout.fillWidth: true
          }

          Button {
             icon.source: "../icons/app/32x32/org.o20.word.svg"
             icon.color: "transparent"
             icon.width: 32
             icon.height: 32
          }

          Button {
             icon.source: "../icons/app/32x32/org.o20.openpoint.svg"
             icon.color: "transparent"
             icon.width: 32
             icon.height: 32
             enabled: false
          }

          Button {
             icon.source: "../icons/app/32x32/org.o20.notes.svg"
             icon.color: "transparent"
             icon.width: 32
             icon.height: 32
             enabled: false
          }

          Item {
            Layout.fillWidth: true
          }
      }
   }
}
