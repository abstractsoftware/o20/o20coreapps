/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QScreen>
#include <QFile>
#include <QStyleFactory>
#include <QGraphicsDropShadowEffect>

#define O20_NOIO

#include <QQmlEngine>
#include <QQuickStyle>
#include <QQuickView>
#include <QQuickItem>
#include <QTest>

#include "O20Gui.h"

#include <O20>
#include <O20Application.h>
#include <O20AsciiArt.h>

#include <cstdio>

int main(int argc, char *argv[])
{
    O20::generateStartupArt();
    
    QCoreApplication::setOrganizationName("Abstract Software Project");
    QCoreApplication::setApplicationName("org.o20.word");
    QCoreApplication::setApplicationVersion("20.1.3");

    O20::ApplicationMetadata metaData;
    metaData.appName = "O20.Notebook";
    metaData.appColor = "#7118a2";
    metaData.appIcon = "qrc:/icons/rebrand/ms-onenote.svg";

    return O20::O20_OfficeApp(argc, argv, metaData);
}
