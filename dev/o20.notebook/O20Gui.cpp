#include "O20Gui.h"
#include "src/ui_O20Ui.h"
#include <converter.h>

#include <QTest>
#include <QQuickItem>
#include <QQuickView>
#include <QtWidgets>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include <O20>

/*
 Initialize O20.Word
 */

O20Gui::O20Gui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::O20Gui)
{
    // setup user interface
    ui->setupUi(this);

    setWindowTitle("O20.Notebook");
/*    ui->sectionList->clear();
    QListWidgetItem* item = new QListWidgetItem("Quick Notes");
    ui->sectionList->addItem(item);
    ui->pageList->clear();

    notebooksfolder = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/O20/Notebooks/";

    opennotes();
    renamenote();

    QDir mydir = QDir(notebooksfolder);
    if (!mydir.exists())
        mydir.mkdir(notebooksfolder);

    row = 0;

    ui->pageList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->pageList, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showContextMenu(QPoint)));

    connect(ui->notecontents, &QTextEdit::textChanged, this, &O20Gui::savenotes);
    connect(ui->notebookname, &QLineEdit::textChanged, this, &O20Gui::renamenote);
    connect(ui->newpage, &QToolButton::released, this, &O20Gui::createnewnotepage);
    connect(ui->pageList, &QListWidget::currentRowChanged, this, &O20Gui::changenoterow);
}

void O20Gui::showContextMenu(const QPoint &pos)
{
    // Create menu and insert some actions
    QMenu myMenu;
    QPoint globalPos = ui->pageList->viewport()->mapToGlobal(pos);
    QPoint localPos  = ui->pageList->viewport()->mapFromGlobal(pos);

    QAction* action = new QAction(QIcon(":/basic/symbol-cancel_small@1x.png"), "Delete", this);
    myMenu.addAction(action);
    int r = ui->pageList->row(ui->pageList->itemAt(pos));
    if (ui->pageList->count() == 1) action->setEnabled(false);
    connect(action, &QAction::triggered, [=]() { ui->pageList->takeItem(r); notepages.remove(r); });

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void O20Gui::changenoterow() {
    notepages[row] = ui->notecontents->toPlainText();
    notepagesnames[row] = ui->notebookname->text();

    row = ui->pageList->currentRow();
    QString a = notepagesnames[row];
    ui->notecontents->setPlainText(notepages[row]);
    ui->notebookname->setText(a); */
}

/*
void O20Gui::closeEvent(QCloseEvent* event) {
    savenotes();
    event->accept();
}

void O20Gui::createnewnotepage() {
    notepages.append("");
    notepagesnames.append("");
    ui->pageList->addItem("New Section");
    ui->pageList->setCurrentRow(ui->pageList->currentRow()+1);
    ui->notebookname->setText("");
}

void O20Gui::renamenote() {
    if (ui->notebookname->text() == "") ui->pageList->currentItem()->setText("Untitled Page");
    else ui->pageList->currentItem()->setText(ui->notebookname->text());
}

void O20Gui::opennotes() {
	return;
    QFile inputFile(notebooksfolder + "OneNoteSections.note");
    inputFile.open(QIODevice::ReadOnly);
    if (!inputFile.isOpen()) {
        createnewnotepage();
        return;
    }

    QXmlStreamReader stream(&inputFile);

    stream.readNextStartElement();
    stream.readNextStartElement();
    ui->sectionList->currentItem()->setText(stream.attributes().value("id").toString());

    while (stream.readNextStartElement()) {
        if (stream.name() == "PageObjectSpace") {
	    notepagesnames.append(stream.attributes().value("id").toString());

	    if (notepagesnames.last() == "") ui->pageList->addItem("Untitled Page");
	    else ui->pageList->addItem(notepagesnames.last());

            while (stream.readNextStartElement()) {
                if (stream.name() == "jcidTextStyleObject") {
                    notepages.append(stream.readElementText());
                   ui->notecontents->setPlainText(stream.readElementText());
		}
            }
	}
    }

    ui->notecontents->setPlainText(notepages[0]);
    ui->pageList->setCurrentRow(0);
    ui->notebookname->setText(notepagesnames[0]);
}

void O20Gui::savenotes() {
    notepages[row] = ui->notecontents->toPlainText();
    notepagesnames[row] = ui->notebookname->text();

    QFile notebooks(notebooksfolder + "OneNoteSections.note");
    notebooks.open(QIODevice::WriteOnly);
    QXmlStreamWriter notestream(&notebooks);
    notestream.writeStartDocument();

    notestream.writeStartElement("One");
    notestream.writeStartElement("SectionObjectSpace");
    notestream.writeAttribute("id", ui->sectionList->currentItem()->text());

    int index = 0;
    for (QString text : notepages) {
	notestream.writeStartElement("PageObjectSpace");
        notestream.writeAttribute("id", notepagesnames[index]);
        notestream.writeStartElement("jcidTextStyleObject");
        notestream.writeCharacters(text);
        notestream.writeEndElement();
	notestream.writeEndElement();
	++index;
    }

    notestream.writeEndDocument();

    notebooks.close();
}
*/


/* Destructor: delete UI */
O20Gui::~O20Gui()
{
    delete ui;
}

/*#define DOCUMENTS QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
#define HOME      QStandardPaths::writableLocation(QStandardPaths::HomeLocation)

void getOrCreateNotebook() {
    QString docs = qEnvironmentVariableIsSet("SNAP") ? HOME : DOCUMENTS;

    if(!QDir(docs + "/O20/Notebooks/").exists()) {
        QDir().mkdir(docs + "/O20");
        QDir().mkdir(docs + "/O20/Notebooks");
    }
}*/
