#include <QApplication>
#include <QQuickWindow>
#include <QQmlEngine>
#include <QQuickStyle>
#include <QQuickView>
#include <QQmlComponent>

int main(int argc, char** argv) {

       QQuickStyle::setStyle("Material");
       qputenv("QT_STYLE_OVERRIDE","Breeze");
       qputenv("XCURSOR_THEME","breeze_cursors");
       QIcon::setThemeName("breeze");
       QQuickWindow::setDefaultAlphaBuffer(true);

    QApplication a(argc, argv);

    QQuickView *window = new QQuickView(QUrl(QStringLiteral("qrc:/QML/O20SettingsApp.qml")));
    window->resize(400,600);
    window->show();

    return a.exec();
}
