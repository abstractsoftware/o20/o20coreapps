#ifndef SPECTRUMSPEAK_HEADER 
#define SPECTRUMSPEAK_HEADER

#include <QProcess>

class SpectrumSpeak {
public:
    SpectrumSpeak();
    void say(QString what);
    void stop();

private:
    QProcess backend;
    QString os_backend;
    QString voice;
};

#endif
