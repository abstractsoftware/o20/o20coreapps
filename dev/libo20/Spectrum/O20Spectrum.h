#ifndef O20SPECTRUM_HEADER
#define O20SPECTRUM_HEADER

namespace Spectrum {
    class Engine {
        public:
          Engine() {}

          void doEnglishParse(QStringList LIST) {

              for (QString I : LIST) {
                  QString INS = I; I = I.toLower();

                  if (GOFLAG) {
                      if (I == "to") // as in go to line number 1
                          continue;  // SKIP
                      else if (I == "up") GOWHERE = 3;
	              else if (I == "down") GOWHERE = 4;
	              else if (I == "line") GOLINE = 1;
                      else if (I.contains("begin") or I == "start") // as in beginning
                          GOWHERE = 1; // to beginning of line 
                      else if (I.contains("end")) // as in DocumentEnd (stoopid but true)
                          GOWHERE = 2;
                      else if (I == "left" or I == "before" or I == "previous")
                          GOWHERE = 5;
                      else if (I.contains("word"))
                          GOWORD = 1;
                      else if (I == "right" or I == "next" or I == "after")
                          GOWHERE = 6;
                      else {
                          bool isNum = false;
                          int num = I.toInt(&isNum);

                          if (isNum) {
	                      GO_TO = num;
	                      GONUMFLAG = 1;
	                  }
	              }
                  }

                  if (RENAMEFLAG) RENAMETO += INS;

                  if (SELECTFLAG) {
                      if (I == "line") SELECTLINE = 1;
                      else if (I.contains("char")) SELECTCHAR = 1;
	              else if (I.contains("para") or I == "block") SELECTBLOCK = 1;
	              else if (I == "all" or I.contains("doc")) SELECTALL = 1;
                  }

                  if (I == "go") GOFLAG = 1;
                  else if (I == "italic") ITALIC = 1;
                  else if (I == "bold") BOLD = 1;
                  else if (I == "underline") UNDERLINE = 1;
                  else if (I.contains("strike")) STRIKEOUT = 1;
                  else if (I == "left") ALIGNLEFT = 1;
                  else if (I == "right") ALIGNRIGHT = 1;
                  else if (I == "center") ALIGNCENTER = 1;
                  else if (I == "justify") ALIGNJUSTIFY = 1;
                  else if (I == "bullet") BULLETDISK = 1;
                  else if (I == "number") BULLETNUM = 1;
                  else if (I == "select") SELECTFLAG = 1;
                  else if (I == "insert") INSERTFLAG = 1;
                  else if (I == "rename") RENAMEFLAG = 1;
                  else if (I == "overline" or I == "lineover") OVERLINE = 1;
                  else if (I.contains("super")) SUPERSCRIPT = 1;
                  else if (I.contains("sub")) SUBSCRIPT = 1;
                  else if (I == "clear") CLEARFMT = 1;
              }
	  }

          void doChineseParse(QStringList LIST) {
              saysomething = "对不起，我不会看中文。";
	  }	

	public:

	    QString saysomething = "";
	   int GOFLAG = 0, SELECTFLAG = 0, INSERTFLAG = 0;
   int GOLINE = 0, GONUMFLAG  = 0, GOWHERE = 0, GO_TO = 0, GOWORD = 0;
   int ITALIC = 0, BOLD = 0, UNDERLINE = 0, STRIKEOUT = 0, ALIGN = 0, ALIGNLEFT = 0, ALIGNRIGHT = 0, ALIGNCENTER = 0, ALIGNJUSTIFY = 0;
   int BULLET = 0, BULLETDISK = 0, BULLETCIRCLE = 0, BULLETSQUARE = 0, BULLETNUM = 0, BULLETALPHA = 0, BULLETLOWER = 0, BULLETROMAN = 0, BULLETROMANLOWER = 0;
   int STYLE = 0, UPDATESTYLE = 0, STYLENORMAL = 0, STYLEHEAD1 = 0, STYLEHEAD2 = 0, STYLEHEAD3 = 0, STYLEHEAD4 = 0, STYLEHEAD5 = 0, STYLETITLE = 0, STYLESUBTITLE = 0;
   int OVERLINE = 0, SUBSCRIPT = 0, SUPERSCRIPT = 0, CLEARFMT = 0, SELECT = 0;
   int SELECTLINE = 0, SELECTCHAR = 0, SELECTBLOCK = 0, SELECTALL = 0, RENAMEFLAG = 0;
   QString RENAMETO = "";  
    };
};

#endif
