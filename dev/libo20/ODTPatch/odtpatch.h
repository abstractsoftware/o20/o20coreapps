#ifndef ODTPATCHODT_HEADER
#define ODTPATCHODT_HEADER

namespace O20 {
    void PatchODT(QString filename, QStringList stylenames, QMap<QString, QTextCharFormat> scf, QMap<QString, QTextBlockFormat> sbf);
}

#endif
