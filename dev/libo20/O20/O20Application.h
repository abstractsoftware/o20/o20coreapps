#ifndef O20APPLICATION_H
#define O20APPLICATION_H

#include <QApplication>
#include <QString>
#include <QColor>
#include <QIcon>

#include <QApplication>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QScreen>
#include <QFile>
#include <QStyleFactory>
#include <QGraphicsDropShadowEffect>

#include <QLibraryInfo>
#include <QTranslator>

#include <QSettings>
#include <QQmlEngine>
#include <QQuickStyle>
#include <QQuickView>
#include <QQuickItem>
#include <QTest>

namespace O20 {

   /* Defaults are for O20.Word */
   struct ApplicationMetadata {
       QString	appName		= "Word";
       QString	appSplash	= "qrc:/QML/Office20SplashScreen.qml"; // you won't need to change this for most Office apps
       QColor	appColor	= "#2b579a";
       QString	appIcon		= "qrc:/icons/rebrand/ms-word.svg"; //"../AppIcons/blackandwhite/WinWordLogoSmall.scale-180.png";
   };

   int O20_OfficeApp(int argc, char *argv[], ApplicationMetadata mdata) {

       // Create the Breeze styles:
       QQuickStyle::setStyle("Universal");
       qputenv("QT_STYLE_OVERRIDE","Breeze");
       qputenv("XCURSOR_THEME","breeze_cursors");
       //QIcon::setThemeName("Breeze Light");
       QQuickWindow::setDefaultAlphaBuffer(true);

       // For Retina and HighDPI screens
       QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

       // Create a new O20 application and set the Icon
       QApplication o20(argc, argv);
       //o20.setWindowIcon(QIcon(mdata.appIcon));

       QCoreApplication::setOrganizationName("O20 Project");
       QCoreApplication::setOrganizationDomain("io.gitlab.o20");
       QCoreApplication::setApplicationName("o20coreapps");

       QSettings settings;
       settings.beginGroup("UserSettings");
       bool doSplashScreen = settings.value("SplashScreen", true).toBool();
       bool getUserLang = settings.value("DetectLang", true).toBool();
       settings.endGroup();

       QTranslator qtTranslator;
       qtTranslator.load("qt_" + QLocale::system().name(),
              QLibraryInfo::location(QLibraryInfo::TranslationsPath));
       o20.installTranslator(&qtTranslator);

       QTranslator kf5Translator;
       kf5Translator.load("kf5_" + QLocale::system().name(),
              QLibraryInfo::location(QLibraryInfo::TranslationsPath));
       o20.installTranslator(&kf5Translator);

       QTranslator o20Translator;
       o20Translator.load(":/dev/o20_" + QLocale::system().name() + ".qm");

       if (getUserLang)
           o20.installTranslator(&o20Translator);

       // Generate a SplashScreen

       QQuickView* splashScreen = new QQuickView(QUrl(mdata.appSplash));
       splashScreen->setColor(QColor("#00000000"));
       splashScreen->rootObject()->setProperty("appColor", mdata.appColor);
       splashScreen->rootObject()->setProperty("appIcon",  mdata.appIcon);
       splashScreen->rootObject()->setProperty("appName",  mdata.appName);

       splashScreen->setFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::ToolTip);
       splashScreen->setColor("#00000000");

       QRect desktopRect = QGuiApplication::screens()[0]->availableGeometry(); //QApplication::desktop()->availableGeometry();
       QPoint center = desktopRect.center();

       splashScreen->resize(400, 150);
       splashScreen->setX(center.x() - splashScreen->width() * 0.5);
       splashScreen->setY(center.y() - splashScreen->height() * 0.5);

       if (doSplashScreen) splashScreen->show();

       // TODO sometimes seg fault
       QObject::connect(splashScreen->engine(), &QQmlEngine::quit, [=] { std::exit(0); });
       qApp->processEvents();

       // Breeze palette
       QPalette colorScheme;
       colorScheme.setColor(QPalette::Window, QColor("#eff0f1"));
       colorScheme.setColor(QPalette::WindowText, QColor("#414445"));
       colorScheme.setColor(QPalette::Base, QColor("#fcfcfc"));
       colorScheme.setColor(QPalette::AlternateBase, QColor("#f0f0f0"));
       colorScheme.setColor(QPalette::ToolTipBase, QColor("#f0f0f0"));
       colorScheme.setColor(QPalette::ToolTipText, Qt::black);
       colorScheme.setColor(QPalette::Text, QColor("#414445"));
       colorScheme.setColor(QPalette::Button, QColor("#eeeeee"));
       colorScheme.setColor(QPalette::ButtonText, QColor("#26292a"));
       colorScheme.setColor(QPalette::BrightText, Qt::red);
       colorScheme.setColor(QPalette::Link, QColor("#9cc1da"));
       colorScheme.setColor(QPalette::Highlight, QColor("#3daee9"));
       colorScheme.setColor(QPalette::HighlightedText, Qt::white);
       o20.setPalette(colorScheme);

       // application style
       QFile File(":/Style/GlobalAppStyle.css");
       File.open(QFile::ReadOnly);
       QString StyleSheet = QLatin1String(File.readAll());
       qApp->setStyleSheet(StyleSheet);

#ifndef O20_NOIO
       /* Get the input document for ANY o20 format */
       QCommandLineParser parser;
       parser.addPositionalArgument("document", QCoreApplication::translate("main", "O20OpenDocument"));
       parser.process(o20);
       const QStringList args = parser.positionalArguments(); // we will use this later
#endif

       /* O20GuiInterface.BeginCoreServices() */

       if (doSplashScreen) QTest::qWait(500);

       O20Gui o20app;

#ifndef O20_NOIO
       if (args.size() > 0) {
           QString toOpen = args.at(0);
           QString suffix = QFileInfo(toOpen).suffix();

           if (suffix == "docx" or suffix == "dotx" or suffix == "doc" or suffix == "dot") splashScreen->close();
	   if(!o20app.ioReceiver("open:", toOpen)) return 0;
       }
#endif

       splashScreen->close();
       o20app.startCore();
       o20app.show();

       return o20.exec();
   }
}

#endif
