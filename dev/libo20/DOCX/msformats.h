#ifndef MSFORMATS_HEADER
#define MSFORMATS_HEADER

namespace O20 {
    namespace MSFormats {
        // ---> Returns the MS document as HTML
        QString convertMSDocument(QString filename);
	//QString convertRTFDocument(QString filename);
    }
}

#endif
